Feature: Verify audio app home page
    Perform tests on selecting AM/FM bands on audio app home page

    @smoke1
  Scenario: Select a AM Channel from a category
    Given  I select a "AM" band
    And    I tap on browse stations
   # When   I tap on Categories and select "ROCK" category
    Then   I should see a "540" channel under selected category
    When   I select a channel "540"
    Then   I should see channel "Station 540 AM" been selected on home page

  @smoke
  Scenario: Enter a channel using audio tuner
    Given I have changed my device orientation to "landscape"
    Given I select a "FM" band
    When  I tap on manual tuner
    And   I enter channel as "92.7" and confirm
    Then  I should see channel "92.7 FM" been selected on home page

  @smoke
  Scenario Outline:: Select a FM Channel from a category
    Given I select a "<band>" band
    When  I tap on manual tuner
    And   I enter channel as "<channel>" and confirm
    Then  I should see channel "<radioBand>" been selected on home page

    Examples:
      |	band	|	channel	| radioBand	|
      |	FM		|	91.1		|	91.1 FM	|
      |	FM		|	92.7		|	92.7 FM	|
      |	AM		|	550			|	550 AM	|