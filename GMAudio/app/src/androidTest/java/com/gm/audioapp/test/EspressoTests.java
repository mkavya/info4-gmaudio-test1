package com.gm.audioapp.test;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.gm.audioapp.R;
import com.gm.audioapp.ui.activities.NowPlayingActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;


public class EspressoTests {

    @Rule
    public ActivityTestRule<NowPlayingActivity> activityTestRule = new ActivityTestRule<>(NowPlayingActivity.class);

    private Activity activity;

    @Before
    public void setup() {
        activityTestRule.launchActivity(new Intent());
        activity = activityTestRule.getActivity();
    }

    @After
    public void tearDown() {
        activityTestRule.finishActivity();
    }


    @Given("^I select a \"([^\"]*)\" band$")
    public void i_select_a_band(String arg1) {
        SystemClock.sleep(600);
        onView(withContentDescription("Open drawer")).perform(click());
        onView(allOf(withText(arg1), hasSibling(withId(R.id.title)))).perform(click());
    }

    @And("^I tap on browse stations$")
    public void i_tap_on_audio_browse(){

        onView(withId(R.id.img_audio_browse)).perform(click());
    }

    @When("^I tap on Categories and select \"([^\"]*)\" category$")
    public void i_select_a_category(String text){

        //onView(allOf(withText("Categories"),hasSibling(withId(R.id.category_item_metadata)))).perform(click());

        onView(withIndex(withText("Categories"),1)).check(matches(isDisplayed()));
        onView(allOf(withText(text),hasSibling(withIndex(withId(R.id.preset_item_metadata),1))))
               .perform(click());

        SystemClock.sleep(500);
    }

    @Then("^I should see a \"([^\"]*)\" channel under selected category$")
    public void i_see_channel_under_category(String arg1){
        onView(withIndex(allOf(withId(R.id.preset_station_channel),withText(arg1)),0))
                .check(matches(isDisplayed()));
    }

    @When("^I select a channel \"([^\"]*)\"$")
    public void i_select_given_channel(String arg1){
        onView(withIndex(allOf(withId(R.id.preset_station_channel),withText(arg1)),0))
                .perform(click());
    }

    @Then("^I should see Audio title on home page$")
    public void i_should_see_Audio_title_on_home_page() throws Throwable {
    onView(withText("Audio")).check(matches(isDisplayed()));
    }

    @Given("^I have changed my device orientation to \"([^\"]*)\"$")
    public void i_change_device_orientation(String arg1){

        if(arg1.equalsIgnoreCase("LANDSCAPE")) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        else
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        SystemClock.sleep(2000);
    }

    @Given("^I run swipe operations$")
    public void i_run_swipe_operations()  {

        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        onView(withId(R.id.img_audio_browse)).perform(click());
        //onView(allOf(withText("Categories"),hasSibling(withId(R.id.category_item_metadata)))).perform(click());
        onView(allOf(withIndex(withId(R.id.page_down),1))).perform(click());
        onView(allOf(withText("CLASSICAL"),hasSibling(withIndex(withId(R.id.preset_item_metadata),1))))
                .perform(click());

        /*onView(withIndex(allOf(withId(R.id.preset_station_channel),withText("550")),0))
                .perform(click());*/
    }


    @When("^I tap on manual tuner$")
    public void i_tap_manual_tuner(){
        onView(withId(R.id.img_audio_tune_to))
                .check(matches(isDisplayed()))
                .perform(click());
    }

    @And("^I enter channel as \"([^\"]*)\" and confirm$")
    public void i_enter_channel_number(String input){
        String channel = input.replaceAll("\\D+", "");
        System.out.println("Channel to enter :" + input);
        for(int i =0;i < channel.length();i++) {
            SystemClock.sleep(200);
           switch (channel.charAt(i)) {
                case '0':
                    onView(withId(R.id.ui_0_short)).perform(click());
                    SystemClock.sleep(200);
                    break;
                case '1':
                    onView(withId(R.id.ui_1_short)).perform(click());
                    SystemClock.sleep(200);
                    break;
                case '2':
                    onView(withId(R.id.ui_2_short)).perform(click());
                    break;
                case '3':
                    onView(withId(R.id.ui_3_short)).perform(click());
                    break;
                case '4':
                    onView(withId(R.id.ui_4_short)).perform(click());
                    break;
                case '5':
                    onView(withId(R.id.ui_5_short)).perform(click());
                    break;
                case '6':
                    onView(withId(R.id.ui_6_short)).perform(click());
                    break;
                case '7':
                    onView(withId(R.id.ui_7_short)).perform(click());
                    break;
                case '8':
                    onView(withId(R.id.ui_8_short)).perform(click());
                    break;
                case '9':
                    onView(withId(R.id.ui_9_short)).perform(click());
                    break;
                    default:
                        System.out.println("Selected number :" + channel.charAt(i));

            }
        }

                onView(withId(R.id.img_ui_audio_tune))
                        .check(matches(isDisplayed()))
                        .perform(click());
    }

    @Then("^I should see channel \"([^\"]*)\" been selected on home page$")
    public void i_should_see_channel_on_home_page(String arg1){
        String [] input = arg1.split("\\s+");
        System.out.println("Frequency = " + input[0]);
        System.out.println("type = " + input[1]);


        onView(allOf(withId(R.id.metadata_line1_frequency),withText(input[0] )))
                .check(matches(isDisplayed()));
        onView(allOf(withId(R.id.metadata_line1_rdsstationinfo),withText(input[1])))
                .check(matches(isDisplayed()));
    }

    public static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }
}
