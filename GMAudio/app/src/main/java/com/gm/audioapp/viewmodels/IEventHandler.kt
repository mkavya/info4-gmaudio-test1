package com.gm.audioapp.viewmodels

import android.view.KeyEvent
import android.view.View

interface IEventHandler {
    fun onClickHandler(view: View)
    fun onClickHandler(view:View, any: Any)
    fun onKeyHandler(keyCode: Int, event: KeyEvent?)
    fun onVoiceHandler(vararg voiceControlCommands: String)
    fun onRemoteHandler(remoteControlCommands: String)
}