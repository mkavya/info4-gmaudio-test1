package com.gm.audioapp.sdkintegration

import android.text.TextUtils
import android.util.Log
import com.gm.audioapp.sdkintegration.mock.DataSourceProvider
import com.gm.audioapp.ui.customclasses.SharedPreferencesController
import com.gm.audioapp.viewmodels.*
import com.gm.audioapp.viewmodels.DataPoolDataHandler.browseList
import com.gm.audioapp.viewmodels.DataPoolDataHandler.favoriteList
import com.gm.audioapp.viewmodels.DataPoolDataHandler.aMFMStationInfo_t
import com.gm.audioapp.viewmodels.DataPoolDataHandler.browseListAM
import com.gm.audioapp.viewmodels.DataPoolDataHandler.browseListFM
import java.util.*

class SimulationManager: IManager {


    override fun onNowplayingPlayPause() {
    }

    override fun onNowplayingDirectTune() {
    }

    override fun onAmRequestSource() {
        SystemListener.updateCarouselOnSourceChanged(NOWPLAYING_SOURCE_TYPE.AM)
        onSourceReset(NOWPLAYING_SOURCE_TYPE.AM)
        setCarousalView()

        var station:AMFMStationInfo_t
        if(aMFMStationInfo_t.get() == null || TextUtils.isEmpty(aMFMStationInfo_t.get()?.frequency.toString()) || !aMFMStationInfo_t.get()?.rdsStationInfo!!.equals(NOWPLAYING_SOURCE_TYPE.AM.name))
            station = getRandomStation()
        else
            station= aMFMStationInfo_t.get()!!
//        val station = getRandomStation()
        //SystemListener.onMetadataChanged(station)
        //SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(aMFMStationInfo_t(stationData.frequency!!.toFloat(),stationData.hdStationNameLong,stationData.id!!.toFloat(),stationData.waveband,stationData.ptyCode!!.toFloat(), eRdsStatus(), eTPStationStatus()))
        SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))
        //SystemListener.updateCarouselView(station.metaData1, station.sourceType)
    }

    override fun onFmRequestSource() {
        SystemListener.updateCarouselOnSourceChanged(NOWPLAYING_SOURCE_TYPE.FM)
        onSourceReset(NOWPLAYING_SOURCE_TYPE.FM)
        setCarousalView()
        var station:AMFMStationInfo_t
        if(aMFMStationInfo_t.get() == null || TextUtils.isEmpty(aMFMStationInfo_t.get()?.frequency.toString()) || !aMFMStationInfo_t.get()?.rdsStationInfo!!.equals(NOWPLAYING_SOURCE_TYPE.FM.name))
            station = getRandomStation()
        else
            station= aMFMStationInfo_t.get()!!
//        val station = getRandomStation()
        //SystemListener.onMetadataChanged(station)
        SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))
        //SystemListener.updateCarouselView(station.metaData1, station.sourceType)
    }

    override fun onAMFMTuneRequest(any: Any?) {
        if (any != null) {

            for (position in browseList.indices) {

                if (any.toString().toFloat().toString().equals(browseList[position].frequency.toString())) {
                    val it = browseList[position]
                    if(it.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.AM.name)
                        SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(AMFMStationInfo_t(it.frequency,it.stationName,it.objectId,it.rdsStationInfo,it.ptyCategory, eRdsStatus(), eTPStationStatus()))
                    else if(it.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.FM.name)
                        SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(AMFMStationInfo_t(it.frequency.toFloat(),it.stationName,it.objectId,it.rdsStationInfo,it.ptyCategory, eRdsStatus(), eTPStationStatus()))

                    break
                }
            }

//            val nowPlaying = getRandomStation()

            //SystemListener.onMetadataChanged(AMFMStationInfo_t(nowPlaying.sourceType, any.toString(), nowPlaying.metaData2, nowPlaying.metaData3))
            //SystemListener.updateCarouselView(any.toString(), nowPlaying.sourceType)
        }
    }

    override fun onFavoriteRequest(any: Any?) {
        if (any != null) {
            SharedPreferencesController.loadPreferences()
            SystemListener.updateFavorites(any as AMFMStationInfo_t)
            SharedPreferencesController.savePreferences()
        }
    }

    override fun onMediaRequest() {
        println("USB Request!!!")
        onSourceReset(NOWPLAYING_SOURCE_TYPE.USBMSD)
        //SystemListener.onMetadataChanged(getRandomStation())
    }

    override fun onAuxRequest() {
        println("AUX Request!!!")
        onSourceReset(NOWPLAYING_SOURCE_TYPE.AUX)
        //SystemListener.onMetadataChanged(getRandomStation())
    }

    override fun onBrowseList() {
        println("Simulation Manager onBrowseList!!!")
        if (browseListAM.isEmpty() || browseListFM.isEmpty())
            SystemListener.onStaticStationList(getStaticStationList())
    }


    override fun initAudio() {

        onRequestFavoriteList()

        var station:AMFMStationInfo_t
        if(aMFMStationInfo_t.get() == null || TextUtils.isEmpty(aMFMStationInfo_t.get()?.frequency.toString()))
        station = getRandomStation()
        else
            station= aMFMStationInfo_t.get()!!

        setCarousalView()


        if(station.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))
        else if(station.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.FM.name)
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))


        //SystemListener.onMetadataChanged(station)

        //SystemListener.updateCarouselView(station.metaData1, station.sourceType)
        println("SimulationManager: initAudio")

    }

    override fun onRequestNowPlayingNext() {

        var index = browseList.indexOf(aMFMStationInfo_t.get())
        if (++index< browseList.size) index else index = 0
        val station = browseList[index]

        if(station.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))
        else if(station.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.FM.name)
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))


        //SystemListener.onMetadataChanged(station)
        //SystemListener.updateCarouselView(station.metaData1, station.rdsStationInfo)
    }

    override fun onRequestNowPlayingPrevious() {
        var index = browseList.indexOf(aMFMStationInfo_t.get())
        if (--index >= 0) index else index = browseList.size-1
        val station = browseList[index]

        if(station.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))
        else if(station.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.FM.name)
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(AMFMStationInfo_t(station.frequency,station.stationName,0f,station.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus()))

        //SystemListener.onMetadataChanged(station)
        //SystemListener.updateCarouselView(station.metaData1, station.rdsStationInfo)
    }

    private fun setCarousalView(){
        onBrowseList()
        SystemListener.setCarouselView()
    }

    private fun getRandomStation() : AMFMStationInfo_t {
        println("sourceType: ${DataPoolDataHandler.previousSourceType}")

        val list = DataSourceProvider.nowPlayingDataList(DataPoolDataHandler.previousSourceType)
//        return list[Random().nextInt(list.size)]
        return list[0]
    }

    private fun getStaticStationList(): AMFMStationList {
        val list = DataSourceProvider.nowPlayingDataList(DataPoolDataHandler.previousSourceType)
        list.forEachIndexed({index, nowPlayingData->
            kotlin.run {
                list[index].isFavorite = favoriteList.contains(nowPlayingData.frequency.toString())
            }
        })
        return AMFMStationList(list.size.toString(),list)
    }
    override fun onRequestFavoriteList() {
//        var favList=ArrayList<String>()
//        favList.add("98.3")
//        favList.add("Hold On")
//        favList.add("950")
//        favList.add("How Deep Is Your Love")
//        favList.add("Despacito")
//        favList.add("Rock On!")
//        print("FavList Size $favList.size")
//        SystemListener.onFavListChange(favList)

        SharedPreferencesController.loadPreferences()
    }
    override fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?) {
        println(" Source Reset!!!! ${DataPoolDataHandler.previousSourceType}")
        when(DataPoolDataHandler.previousSourceType){
            NOWPLAYING_SOURCE_TYPE.AM->{
                resetTuner()
            }
            NOWPLAYING_SOURCE_TYPE.FM->{
                resetTuner()
            }
            NOWPLAYING_SOURCE_TYPE.USBMSD,NOWPLAYING_SOURCE_TYPE.AUX->{
                resetMedia()
            }

        }
        SystemListener.onCurrentSourceChanged(sourceTYPE!!)
    }

    private fun resetMedia() {
    }

    private fun resetTuner() {
        Log.v("SeekRelease!.....","resetTuner")
    }


}