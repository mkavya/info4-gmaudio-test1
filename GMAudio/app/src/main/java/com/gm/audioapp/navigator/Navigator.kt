package com.gm.audioapp.navigator

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import com.gm.audioapp.ui.activities.BaseActivity

interface Navigator {

    fun finishActivity()
    fun startActivity(intent: Intent)
    fun startActivity(action: String)
    fun startActivity(action: String, args: Bundle)
    fun startActivity(action: String, uri: Uri)
    fun startActivity(activityClass: Class<out BaseActivity>)
    fun startActivity(activityClass: Class<out BaseActivity>, args: Bundle)
    fun startActivity(activityClass: Class<out BaseActivity>, args: Parcelable)
    fun startActivity(activityClass: Class<out BaseActivity>, arg: String)
    fun startActivity(activityClass: Class<out BaseActivity>, arg: Int)

    fun startActivityForResult(activityClass: Class<out BaseActivity>, requestCode: Int)
    fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: Parcelable, requestCode: Int)
    fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: String, requestCode: Int)
    fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: Int, requestCode: Int)

    fun replaceFragment(@IdRes containerId: Int, fragment: Fragment, args: Bundle)
    fun replaceFragment(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle)
    fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, args: Bundle, backstackTag: String)
    fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle, backstackTag: String)

}
