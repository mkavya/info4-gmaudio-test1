package com.gm.audioapp.ui.activities.fragments

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import com.android.car.view.PagedListView
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.GMAudioApp.Companion.animationManager
import com.gm.audioapp.GMAudioApp.Companion.radioPresetsFragment


import com.gm.audioapp.R
import com.gm.audioapp.databinding.RadioPresetsListBinding
import com.gm.audioapp.ui.animations.PresetListScrollListener
import com.gm.audioapp.ui.animations.RadioAnimationManager
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.EventProcessor
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE

/**
 * A fragment that functions as the main display of the information relating to the current radio
 * station. It also displays controls that allows the user to switch to different radio stations.
 */
open class RadioPresetsFragment : Fragment(), FragmentWithFade,  RadioAnimationManager.OnExitCompleteListener {

    private val TAG = "PresetsFragment"
    private val ANIM_DURATION_MS = 200
    private var mRootView: View? = null
    private var mCurrentRadioCard: View? = null
    private var mMainDisplay: View? = null
    private val evG_Browse_Init = "onBrowseList"

    var binding: RadioPresetsListBinding?= null
    private var mPresetsList: PagedListView? = null
    private var mAnimManager: RadioAnimationManager? = null
    // private var mPresetListExitListener: PresetListExitListener? = null

    /**
     * Interface for a class that will be notified when the button to open the list of the user's
     * favorite radio stations has been clicked.
     */
    /*interface RadioPresetListClickListener {
        *//**
     * Method that will be called when the preset list button has been clicked. Clicking this
     * button should open a display of the user's presets.
     *//*
        fun onPresetListClicked()
    }*/

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: RadioPresetsListBinding = DataBindingUtil.inflate(
                inflater, R.layout.ics_audio_am_station_list, container, false)
        mRootView = binding.root
        mCurrentRadioCard = mRootView?.findViewById(R.id.current_radio_station_card)

        mPresetsList = mRootView?.findViewById<PagedListView>(R.id.stations_list)
        setPresetsListMarginTop(mPresetsList)
        mPresetsList?.setLightMode()
        mPresetsList?.getLayoutManager()?.setOffsetRows(false)
        mPresetsList?.getRecyclerView()?.addOnScrollListener(PresetListScrollListener(
                context, mRootView, mCurrentRadioCard, mPresetsList))


        radioPresetsFragment=this

        binding.let {
            it.dataPoolHandler = DataPoolDataHandler
            it.clickHandler = EventHandler
        }



        mAnimManager = RadioAnimationManager(context, mRootView)
        animationManager=mAnimManager!!
        mAnimManager?.playEnterAnimation()

        EventHandler.initEvent(evG_Browse_Init)

        return mRootView
    }


    fun setPresetsListMarginTop(mPresetsList: PagedListView?) {

        val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        var display = windowManager.defaultDisplay
        val dis = DisplayMetrics()
        display.getMetrics(dis)
        val marginTop = dis.densityDpi/2

        val params = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        )
        val mPresetFinalHeight = GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.car_preset_item_height)
        params.setMargins(0, marginTop + mPresetFinalHeight + 20 , 0, 0)
        mPresetsList?.setLayoutParams(params)
    }

    override fun fadeOutContent() {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(mCurrentRadioCard, View.ALPHA, 0f)
        containerAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        containerAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}

            override fun onAnimationEnd(animation: Animator) {
                Log.d(TAG, "Manish onAnimationEnd(): mCurrentRadioCard")
                mCurrentRadioCard?.setVisibility(View.GONE)
            }

            override fun onAnimationCancel(animation: Animator) {}

            override fun onAnimationRepeat(animation: Animator) {}
        })

        val presetListAlphaAnimator = ObjectAnimator.ofFloat(mPresetsList, View.ALPHA, 0f)
        presetListAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        presetListAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}

            override fun onAnimationEnd(animation: Animator) {
                Log.d(TAG, "Manish onAnimationEnd(): mPresetsList")
                mPresetsList?.setVisibility(View.GONE)
            }

            override fun onAnimationCancel(animation: Animator) {}

            override fun onAnimationRepeat(animation: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(containerAlphaAnimator, presetListAlphaAnimator)
        animatorSet.start()
    }

    override fun fadeInContent() {
        // Only the current radio card needs to be faded in as that is the only part
        // of the fragment that will peek over the manual tuner.
        val containerAlphaAnimator = ObjectAnimator.ofFloat(mCurrentRadioCard, View.ALPHA, 1f)
        containerAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        containerAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                Log.d(TAG, "Manish onAnimationStart(): mCurrentRadioCard")
                mCurrentRadioCard?.setVisibility(View.VISIBLE)
                //mCurrentRadioCard?.setVisibility(View.GONE)
            }

            override fun onAnimationEnd(animation: Animator) {}

            override fun onAnimationCancel(animation: Animator) {}

            override fun onAnimationRepeat(animation: Animator) {}
        })

        val presetListAlphaAnimator = ObjectAnimator.ofFloat(mPresetsList, View.ALPHA, 1f)
        presetListAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        presetListAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                Log.d(TAG, "Manish onAnimationStart(): mPresetsList")
                //mPresetsList?.setVisibility(View.VISIBLE)
                mPresetsList?.setVisibility(View.GONE)
            }

            override fun onAnimationEnd(animation: Animator) {}

            override fun onAnimationCancel(animation: Animator) {}

            override fun onAnimationRepeat(animation: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(containerAlphaAnimator, presetListAlphaAnimator)
        animatorSet.start()
    }

    override fun onStart() {
        super.onStart()
        //fadeInContent()
    }

    override fun onStop() {
        super.onStop()
        // mAnimManager?.playExitAnimation()
        //fadeOutContent()


    }

    override fun onDestroy() {
        //        mPresetListListener = null;
        super.onDestroy()
    }

    override fun onExitAnimationComplete() {
        Log.d(TAG, "Manish onExitAnimationComplete():")
        radioPresetsFragment = null
        animationManager = null
        if (DataPoolDataHandler.aMFMStationInfo_t.get()!!.rdsStationInfo.equals(NOWPLAYING_SOURCE_TYPE.AM.name))
            EventProcessor.triggerEvent("process_fragment", "onAmRequestSource", null)
        else
            EventProcessor.triggerEvent("process_fragment", "onFmRequestSource", null)

        //EventProcessor.replaceScreen("AMRadioFragment", null)
    }
}
