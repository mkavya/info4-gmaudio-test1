package com.gm.audioapp.sdkintegration

import android.util.Log
import com.gm.audioapp.sdkintegration.RadioManager
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE


/**
 *
 * Created by Aswin on 3/14/2018.
 */
class AndroidManager: IManager {

    private var mRadioManager: RadioManager?=null

    override fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?) {
    }

    @Throws(Exception::class)
    private fun registerRadioListener() {
        Log.d("Tag", "registerTunerListener...")
        // If tuner manager is null then initialise it
        try {
            if (mRadioManager == null) {
                mRadioManager = RadioManager()
            }

        } catch (e: ExceptionInInitializerError) {
            e.printStackTrace()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }


    override fun onNowplayingPlayPause() {
    }

    override fun onNowplayingDirectTune() {
    }

    override fun onAmRequestSource() {

        if(mRadioManager!=null)
        {
            mRadioManager?.setCurrentRadioBand(RadioManager.BAND_AM)
        }

    }

    override fun onFmRequestSource() {
        if(mRadioManager!=null)
        {
            mRadioManager?.setCurrentRadioBand(RadioManager.BAND_FM)
        }
    }

    override fun onAMFMTuneRequest(any: Any?) {
    }

    override fun onFavoriteRequest(any: Any?) {
    }

    override fun onMediaRequest() {
    }

    override fun onAuxRequest() {
    }

    override fun onBrowseList() {
    }
    override fun initAudio() {
        println("AndroidManager: initAudio")
    }

    override fun onRequestNowPlayingNext() {
    }

    override fun onRequestNowPlayingPrevious() {
    }

    override fun onRequestFavoriteList() {
    }

    fun isAndroidSDK(): Boolean = false
}