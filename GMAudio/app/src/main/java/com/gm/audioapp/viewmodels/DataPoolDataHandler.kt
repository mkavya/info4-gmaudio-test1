package com.gm.audioapp.viewmodels

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import com.gm.audioapp.ui.adapters.CarouselRecyclerViewAdapter

/**
 *
 * Created by GM on 3/7/2018.
 */
 object DataPoolDataHandler {

    var previousSourceType: NOWPLAYING_SOURCE_TYPE = NOWPLAYING_SOURCE_TYPE.AM
    var browseList = ObservableArrayList<AMFMStationInfo_t>()
    var browseListAM = ObservableArrayList<AMFMStationInfo_t>()
    var browseListFM = ObservableArrayList<AMFMStationInfo_t>()
    var aMFMStationInfo_t = ObservableField<AMFMStationInfo_t>()
    var favoriteList = ObservableArrayList<String>()
    var viewPager: Int = 0
    var themeType = ObservableField<Boolean>()
    var deviceData = ObservableField<DeviceData>()
    var carouselRecyclerViewAdapter: CarouselRecyclerViewAdapter?=null

    var color = ObservableField<Int>()

  /*  // Generic XML implementation
    var metaData1 = ObservableField<String>()
    var metaData2 = ObservableField<String>()
*/
    //AM variables
   /* var amtunerCurrentstationinfoFrequency: String by DelegateField(metaData1)
    var amTunerCurrentStationInfoRDSStationInfo: String by DelegateField(metaData2)*/
/*

    var AMTUNER_CURRENTSTATIONINFO_FREQUENCY: String by DelegateField(metaData1)
    var AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO: String by DelegateField(metaData2)
*/


    //FM variables
   /* var fmtunerCurrentstationinfoFrequency: String by DelegateField(metaData1)
    var fmTunerCurrentStationInfoRDSStationInfo: String by DelegateField(metaData2)*/
/*
    var FMTUNER_CURRENTSTATIONINFO_FREQUENCY: String by DelegateField(metaData1)
    var FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO: String by DelegateField(metaData2)*/


   /* private class DelegateField(var data: ObservableField<String>) {

        operator fun getValue(thisRef: Any?, property: KProperty<*>) : String {
            return property.name
        }

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
            data.set(value)
        }
    }
*/
    var manualTuneData = ObservableField<ManualTuneData>()

    //DataPoolDataHandler from Low Radio
    var AUDIOMANAGER_CHANGE_AUDIOSOURCE = ObservableField<NOWPLAYING_SOURCE_TYPE>( NOWPLAYING_SOURCE_TYPE.AM)
    var AMTUNER_CURRENTSTATIONINFO_OBJECTID = ObservableField<Float>()
    var AMTUNER_CURRENTSTATIONINFO_FREQUENCY = ObservableField<String>()
    var AMTUNER_CURRENTSTATIONINFO_STATIONNAME = ObservableField<String>()
    var AMTUNER_CURRENTSTATIONINFO_RDSSTATUS = ObservableField<Float>()
    var AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO = ObservableField<String>()
    var AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY = ObservableField<Float>()
    var AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS = ObservableField<Float>()
    var FMTUNER_CURRENTSTATIONINFO_OBJECTID = ObservableField<Float>()
    var FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY = ObservableField<Float>()
    var FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS = ObservableField<Float>()
    var FMTUNER_CURRENTSTATIONINFO_RDSSTATUS = ObservableField<Float>()
    var FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO = ObservableField<String>()
    var FMTUNER_CURRENTSTATIONINFO_FREQUENCY = ObservableField<String>()
    var FMTUNER_CURRENTSTATIONINFO_STATIONNAME = ObservableField<String>()
    var FMTUNER_TPSTATUS_NOWPLAYING = ObservableField<Float>()
    var FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING = ObservableField<Float>()
    var FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE = ObservableField<Float>()
    var FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT = ObservableField<Int>()
    var AMTUNER_STRONGSTATIONS_LIST_LISTID = ObservableField<Float>()
    var AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS = ObservableField<Float>()
    var FMTUNER_STRONGSTATIONS_LIST_LISTID = ObservableField<Float>()
    var FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS = ObservableField<Float>()
    var AMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS = ObservableField<Int>()
    var FMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS = ObservableField<Int>()
    var AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY = ObservableField<String>()
    var AMTUNER_DIRECTTUNE_AVAILABLE_KEYS = ObservableField<String>()
    var FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY = ObservableField<String>()
    var FMTUNER_DIRECTTUNE_AVAILABLE_KEYS = ObservableField<String>()
    var FMTUNER_RDSSWITCH = ObservableField<Int>()
    var FMTUNER_REGIONSETTING = ObservableField<Int>()
    var FMTUNER_TPSTATUS = ObservableField<Int>()
    var FMTUNER_TPSTATUS_SEARCHSTATE = ObservableField<Float>()
    var FMTUNER_TRAFFICALERT_NAME = ObservableField<String>()
    var FMTUNER_TRAFFICALERTACTIVECALL_NAME = ObservableField<String>()
    var FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER = ObservableField<Float>()
    var FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME = ObservableField<String>()
    var FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT = ObservableField<String>()
    var FMTUNER_STATIONAVAILABILITY_METADATA = ObservableField<String>()
    var FMTUNER_STATIONAVAILABILITY_STATUS = ObservableField<Float>()
}