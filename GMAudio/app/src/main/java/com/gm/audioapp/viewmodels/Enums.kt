package com.gm.audioapp.viewmodels

/**
 *
 * Created by Aswin on 3/19/2018.
 */


enum class EQ_MODE_TYPE(val value: Number) {
    EQ_TALK(0),
    EQ_ROCK(1),
    EQ_JAZZ(2),
    EQ_POP(3),
    EQ_COUNTRY(4),
    EQ_CLASSICAL(5),
    EQ_OFF(6),
    EQ_MAX_SIZE(7)
}

enum class TUNER_TYPE_E(val value: Number) {
    eSINGLE_TUNER(0x01),
    eDUAL_TUNER(0x02),
    eNO_TUNER_TYPE(0x03)
}

enum class RADIO_MODE(val value: Number) {
    RADIO_FM(0),
    RADIO_AM(1),
    RADIO_XM(2),
    RADIO_DAB(3),
    RADIO_MAX_SIZE(4)
}

enum class XM_LIST_TYPE(val value: Number) {
    XM_LIST_NONE(0),
    XM_LIST_CAT(1),
    XM_LIST_CHN(2),
    XM_LIST_SIZE(3)
}

enum class PRESET_TYPE(val value: Number) {
    PRESET_FAVOURITE(0),
    PRESET_AS(1)
}

enum class RDS_TYPE(val value: Number) {
    RDS_NONE(0), ///< calibaration default
    RDS_EXTEND(1), ///< US, CANADA
    RDS_FULL(2), ///< EU
    RDS_EXTEND_2(3) ///< AU, NZ, Brazil
}
enum class NOWPLAYING_SOURCE_TYPE(val value: Number) {
    AM(0){
        override fun toString(): String {
            return "AM Stations"
        }
    },
    FM(1){
        override fun toString(): String {
            return "FM Stations"
        }
    },
    USBMSD(2){
        override fun toString(): String {
            return "USB"
        }
    },
    AUX(3){
        override fun toString(): String {
            return "AUX"
        }
    }
}

//## type eTPStationState
enum class eTPStationState {
    AMFM_TP_SEARCH_INITIATED,
    AMFM_TP_SEARCH_COMPLETED
}
enum class eFMStationAvailability
{
    FM_STATION_AVAILABLE,
    FM_STATION_NOT_AVAILABLE,
}
//## type eRDSPtyNACategory
enum class eRDSPtyNACategory(val value: Number) {
    AMFM_RDS_PTY_NA_ALL( 0),
    AMFM_RDS_PTY_NA_POP ( 1),
    AMFM_RDS_PTY_NA_ROCK ( 2),
    AMFM_RDS_PTY_NA_TALK ( 3),
    AMFM_RDS_PTY_NA_COUNTRY ( 4),
    AMFM_RDS_PTY_NA_CLASSICAL ( 5),
    AMFM_RDS_PTY_NA_JAZZ (6)
}





