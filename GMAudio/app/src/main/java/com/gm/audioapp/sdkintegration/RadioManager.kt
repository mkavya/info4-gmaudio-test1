package com.gm.audioapp.sdkintegration

import android.os.Parcel
import android.os.Parcelable
import com.gm.audioapp.viewmodels.AMFMStationInfo_t
import com.gm.audioapp.viewmodels.eRdsStatus
import com.gm.audioapp.viewmodels.eTPStationStatus
import java.util.ArrayList

class RadioManager {


    private var mCurrentRadioBand: Int = INVALID_RADIO_BAND
    private var mAmBandString: String = "AM"
    private var mFmBandString: String = "FM"

/*
    public static final int REGION_ITU_1  = 0;
    */
    /** Americas.
     * @see BandDescriptor *//*
    public static final int REGION_ITU_2  = 1;
    */
    /** Russia.
     * @see BandDescriptor *//*
    public static final int REGION_OIRT   = 2;
    */
    /** Japan.
     * @see BandDescriptor *//*
    public static final int REGION_JAPAN  = 3;*/


    companion object {
        var INVALID_RADIO_BAND: Int = 0
        var BAND_AM: Int = 1
        var BAND_FM: Int = 2
        var mBands = emptyArray<BandDescriptor>()
        var REGION_ITU_1: Int = 0
        var REGION_ITU_2: Int = 1
        var REGION_OIRT: Int = 2
        var REGION_JAPAN: Int = 3
    }

    class ModuleProperties() : Parcelable {

        constructor(parcel: Parcel) : this() {


            var tmp = parcel.readParcelableArray(BandDescriptor.javaClass.classLoader)
            for (i in 0 until tmp.size) {
                mBands[i] = tmp[i] as BandDescriptor

            }

        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {

        }

        override fun describeContents(): Int {
            return 0
        }


        companion object CREATOR : Parcelable.Creator<ModuleProperties> {
            override fun createFromParcel(parcel: Parcel): ModuleProperties {
                return ModuleProperties(parcel)
            }

            override fun newArray(size: Int): Array<ModuleProperties?> {
                return arrayOfNulls(size)
            }
        }
    }

    open class BandDescriptor() : Parcelable {
        constructor(parcel: Parcel) : this() {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {

        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<BandDescriptor> {
            override fun createFromParcel(parcel: Parcel): BandDescriptor {
                var type: Int = lookupTypeFromParcel(parcel)
                when (type) {
                    BAND_AM -> return FmBandDescriptor(parcel)
                    BAND_FM -> return AmBandDescriptor(parcel)

                }



                return BandDescriptor(parcel)
            }


            private fun lookupTypeFromParcel(parcel: Parcel): Int {
                var pos: Int = parcel.dataPosition()
                parcel.readInt()
                var type: Int = parcel.readInt()
                parcel.setDataPosition(pos)
                return type

            }

            override fun newArray(size: Int): Array<BandDescriptor?> {
                return arrayOfNulls(size)
            }
        }

    }

    class AmBandDescriptor(parcel: Parcel) : BandDescriptor(parcel) {

    }

    class FmBandDescriptor(parcel: Parcel) : BandDescriptor(parcel) {
        override fun writeToParcel(parcel: Parcel, flags: Int) {
            super.writeToParcel(parcel, flags)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<FmBandDescriptor> {
            override fun createFromParcel(parcel: Parcel): FmBandDescriptor {
                return FmBandDescriptor(parcel)
            }

            override fun newArray(size: Int): Array<FmBandDescriptor?> {
                return arrayOfNulls(size)
            }
        }

    }


    fun setCurrentRadioBand(radioBand: Int) {
        if (mCurrentRadioBand === radioBand) {
            return
        }

        mCurrentRadioBand = radioBand

        when (radioBand) {
            BAND_AM -> getAmStationList()
            BAND_FM -> getFmStationList()

        }


    }

    private fun getAmStationList(): ArrayList<AMFMStationInfo_t> {

        val stationList = ArrayList<AMFMStationInfo_t>()
        stationList.add(AMFMStationInfo_t(0f,"",0f, "", 0f, eRdsStatus(), eTPStationStatus(), false))
        return stationList
    }

    private fun getFmStationList(): ArrayList<AMFMStationInfo_t> {

        val stationList = ArrayList<AMFMStationInfo_t>()
        stationList.add(AMFMStationInfo_t(0f,"",0f, "", 0f, eRdsStatus(), eTPStationStatus(), false))
        return stationList
    }


}