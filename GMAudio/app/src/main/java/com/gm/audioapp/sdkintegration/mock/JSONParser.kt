package com.gm.audioapp.sdkintegration.mock

import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.viewmodels.*
import org.json.JSONArray
import org.json.JSONObject

/**
 *
 * Created by Aswin on 3/21/2018.
 */

class JSONParser : IDataSourceProvider {


    override fun nowPlayingDataList(): ArrayList<AMFMStationInfo_t> {
        val stationList = ArrayList<AMFMStationInfo_t>()
        val stream = GMAudioApp.appContext.resources.openRawResource(R.raw.simulationdata)
        val jsonArray = JSONArray(EventProcessor.getJsonString(stream))

        for (i in 0 until jsonArray.length()) {
            if (getAMFMStationInfo_t(jsonArray.getJSONObject(i)) != null)
                stationList.add(getAMFMStationInfo_t(jsonArray.getJSONObject(i))!!)
        }

        return stationList
    }

    private fun getAMFMStationInfo_t(jsonObject: JSONObject): AMFMStationInfo_t? {

        if (jsonObject.getString("sourceType").equals("AM") || jsonObject.getString("sourceType").equals("FM")) {
            return AMFMStationInfo_t(jsonObject.getString("metaData1").toFloat(), jsonObject.getString("metaData3"), 0f, jsonObject.getString("sourceType"), 0f, eRdsStatus(), eTPStationStatus())
        }else{
            return null
        }
    }
}