package com.gm.audioapp.sdkintegration.mock

import com.gm.audioapp.viewmodels.AMFMStationInfo_t
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE

/**
 *
 * Created by Aswin on 3/21/2018.
 */

object DataSourceProvider {

    var sourceData: IDataSourceProvider = JSONParser()

    fun nowPlayingDataList(source_TYPE: NOWPLAYING_SOURCE_TYPE?): List<AMFMStationInfo_t> {
        if (source_TYPE == NOWPLAYING_SOURCE_TYPE.AM || source_TYPE == NOWPLAYING_SOURCE_TYPE.FM)
            return sourceData.nowPlayingDataList().filter { it.rdsStationInfo == source_TYPE.name }.sortedBy {it.frequency.toDouble()}
        else
            return sourceData.nowPlayingDataList().filter { it.rdsStationInfo == source_TYPE!!.name }
    }
}