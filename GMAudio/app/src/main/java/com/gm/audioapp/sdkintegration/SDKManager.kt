package com.gm.audioapp.sdkintegration

import android.graphics.Bitmap
import android.util.Log
import com.gm.audioapp.ui.customclasses.FavoriteDataProcessor
import com.gm.audioapp.viewmodels.*
import com.gm.audioapp.viewmodels.DataPoolDataHandler.aMFMStationInfo_t
import gm.media.CMediaPlayer
import gm.media.enums.*
import gm.media.exceptions.ExceptionDeviceFunctionNotEnabled
import gm.media.exceptions.ExceptionInitializationFailed
import gm.media.exceptions.ExceptionPlayFunctionNotEnabled
import gm.media.interfaces.*
import gm.media.utilities.MediaRunnable
import gm.tuner.*

@Suppress("unused")
/**
 *
 * Handles api calls from GM SDK. Includes Radio tuner and other media sources.
 *
 * Created by Aswin on 3/14/2018.
 */
class SDKManager: IManager, TunerListener, IDeviceListener, IPlaybackListener {

    private var mTunerManager: TunerManager?=null
    private var mTunerIntialisationState:Int? = 0
    private val mTAG = SDKManager::class.simpleName
    private var mIDevice: IDevice? = null
    private var mCMediaPlayer: CMediaPlayer? = null

    @Throws(Exception::class)
    private fun registerTunerListener() {
        Log.d(mTAG, "registerTunerListener...")
        // If tuner manager is null then initialise it
        try {
            if (mTunerManager == null) {
                Log.d(mTAG, "before TunerManager initialized ")
                mTunerManager = TunerManager()
                Log.d(mTAG, "after  TunerManager initialized ")
            }
            // Check the status and register all the listeners and Restore the last
            // source
            if (mTunerManager != null) {
                Log.d(mTAG, "Tuner Ready Signal : registering Tuner Listener")
                mTunerIntialisationState = mTunerManager?.tunerAudioStatus
                tunerListenerRegistration()
            }

            if(mTunerManager?.currentStation!!.equals(NOWPLAYING_SOURCE_TYPE.AM.name)) {
                setWaveBand(TunerManager.Waveband.AM)
                SystemListener.onCurrentSourceChanged(NOWPLAYING_SOURCE_TYPE.AM)
            }else{
                setWaveBand(TunerManager.Waveband.FM)
                SystemListener.onCurrentSourceChanged(NOWPLAYING_SOURCE_TYPE.FM)
            }
            requestCurrentTunerStation()
        }
        catch (e:ExceptionInInitializerError){
            e.printStackTrace()
        }
        catch (e: TunerException) {
            e.printStackTrace()
        }
        catch (exception:Exception){}
    }

    private val mConfig = IConfig {
        when(it){
            IConfig.enumConfigItems.AudioZone -> EnumAudioZone.MainCabin
            IConfig.enumConfigItems.BrowseReturnEmptyContainers -> true
            IConfig.enumConfigItems.BrowseReturnUnplayables -> true
            IConfig.enumConfigItems.BrowseSupport -> true
            IConfig.enumConfigItems.CinemoLogLevel -> Log.WARN
            IConfig.enumConfigItems.DeviceSupport -> true
            IConfig.enumConfigItems.EnableBrowseArt -> true
            IConfig.enumConfigItems.EnableNowPlayingArt -> false
            IConfig.enumConfigItems.EnableTrackListArt -> true
            IConfig.enumConfigItems.EnableVideoSupport -> false
            IConfig.enumConfigItems.MediaLogLevel -> Log.INFO
            IConfig.enumConfigItems.PlaybackSupport -> true
            IConfig.enumConfigItems.ResumeOnNewTrack -> false
            else -> null
        }
    }

    @Throws(Exception::class)
    private fun registerMediaListener() {
        try {
            mCMediaPlayer = CMediaPlayer(mConfig)
            setupMediaEngine()
        }
        catch (e: ExceptionPlayFunctionNotEnabled) {
            e.printStackTrace()
        }
        catch (e: ExceptionInitializationFailed) {
            e.printStackTrace()
        }
    }

    @Throws(Exception::class)
    private fun setupMediaEngine() {
        try {
            mCMediaPlayer?.addListener(this as IPlaybackListener)
            mCMediaPlayer?.addListener(this as IDeviceListener)
            mCMediaPlayer?.requestDevices()
        }catch (e: ExceptionDeviceFunctionNotEnabled) {
            e.printStackTrace()
        }
        catch (e: ExceptionPlayFunctionNotEnabled) {
            e.printStackTrace()
        }
    }

    fun isTunerAvailable(): Boolean = mTunerManager != null

    private fun tunerListenerRegistration() {
        Log.d(mTAG, "Going to add listener for tuner...")
        try {
            mTunerManager?.addListener(this)
            Log.d(mTAG, "Going to add listener for tuner...")
            onCurrentStationChanged(getCurrentStation())
        } catch (e: TunerServiceStatusException) {
            Log.d(mTAG, "Exception in TunerServiceStatusException..." + e.localizedMessage)
            e.printStackTrace()
        } catch (e: TunerException) {
            Log.i(mTAG,
                    "Tuner Exception while setting tuner listener")
            e.printStackTrace()
        }
        Log.d(mTAG, "going to notifyTunerApiStateChanged ")
    }

    @Throws(TunerException::class)
    fun getCurrentStation(): TunerStation {
        return mTunerManager?.currentStation!!
    }

    @Throws(TunerException::class)
    fun isFMorAM(): NOWPLAYING_SOURCE_TYPE {
       var sourceType:NOWPLAYING_SOURCE_TYPE=NOWPLAYING_SOURCE_TYPE.AM
        when(mTunerManager?.waveband?.name){
            "AM"->{sourceType= NOWPLAYING_SOURCE_TYPE.AM}
            "FM"->{sourceType= NOWPLAYING_SOURCE_TYPE.FM}
        }
        return sourceType
    }

    @Throws(TunerException::class)
    fun getRadioText(): RadioText {
        return mTunerManager?.radioText!!
    }

    @Throws(TunerException::class)
    fun setWaveBand(wband: TunerManager.Waveband) {
        mTunerManager?.waveband = wband
    }

    @Throws(TunerException::class)
    fun setWavebandAndFrequency(wband: TunerManager.Waveband , var2: Int, var3: Int ) {
        mTunerManager?.setWavebandAndFrequency(wband, var2, var3)
    }

    @Throws(TunerException::class)
    fun tuneTo(frequency: Int ) {
        mTunerManager?.tuneTo(frequency)
    }

    @Throws(TunerException::class)
    fun seekCancel() {
        mTunerManager?.seekCancel()
    }

    /**
     * This method is used to request for the current Tuner Station
     */
    fun requestCurrentTunerStation() {
        val station: TunerStation?

        try {
            station = getCurrentStation()
            if (station != null) {
                Log.d(mTAG, "requestCurrentStation : " + getCurrentStation().frequency)
                onCurrentStationChanged(station)
            }
        } catch (e: TunerServiceStatusException) {
            e.printStackTrace()
        } catch (e: TunerException) {
            e.printStackTrace()
        }

    }

    @Throws(TunerException::class)
    fun isHDModeEnabled(): Boolean? {
        return mTunerManager?.isHDModeEnabled
    }

    @Throws(TunerException::class)
    fun isRDSEnabled(): Boolean? {
        return mTunerManager?.isRdsEnabled
    }


    init {
        println("SDKManager: initAudio")
        registerTunerListener()
    }

    private fun processStationList(stationList: MutableList<TunerStation>?){
        val staticList = ArrayList<AMFMStationInfo_t>()
        when (mTunerManager?.waveband) {
            TunerManager.Waveband.AM -> {
                stationList?.forEach {
                    staticList.add(AMFMStationInfo_t(it.frequency!!.toFloat(),it.hdStationNameLong,it.id!!.toFloat(),it.waveband,it.ptyCode!!.toFloat(),eRdsStatus(), eTPStationStatus(), DataPoolDataHandler.favoriteList.contains(it.frequency.toFloat().toString())))
                }
            }
            TunerManager.Waveband.FM -> {
                stationList?.forEach {
                    val freq = it.frequency.toDouble() / 1000
                    staticList.add(AMFMStationInfo_t(freq.toFloat(),it.hdStationNameLong,it.id!!.toFloat(),it.waveband,it.ptyCode!!.toFloat(),eRdsStatus(), eTPStationStatus(), DataPoolDataHandler.favoriteList.contains(freq.toFloat().toString())))
                }
            }
            else -> return //Add other source type here when needed
        }
        SystemListener.onStaticStationList(AMFMStationList(staticList.size.toString(), staticList))
        SystemListener.setCarouselView()
    }

/*SDK Request Listener Implementation*/

    override fun initAudio() {
        println("SDKManager: initAudio")
        //registerTunerListener()
        registerMediaListener()
        onRequestFavoriteList()
        //need to check and remove the below 2 lines. try removing the call to setCarouselView.
        processStationList( mTunerManager?.staticStationList)
        //processStationList( mTunerManager?.availableStationList)
        //SystemListener.setCarouselView()
    }

    override fun onRequestNowPlayingNext() {
        when(aMFMStationInfo_t.get()?.rdsStationInfo) {
            NOWPLAYING_SOURCE_TYPE.AM.name, NOWPLAYING_SOURCE_TYPE.FM.name->{
                mTunerManager?.seekTuneRelease()
                mTunerManager?.seekNextAvailable(TunerManager.Direction.UP)
            }
            NOWPLAYING_SOURCE_TYPE.USBMSD.name -> {
                mCMediaPlayer?.nextTrack(1)
            }
            NOWPLAYING_SOURCE_TYPE.AUX.name -> {

            }
        }
    }

    override fun onRequestNowPlayingPrevious() {
        when(aMFMStationInfo_t.get()?.rdsStationInfo) {
            NOWPLAYING_SOURCE_TYPE.AM.name, NOWPLAYING_SOURCE_TYPE.FM.name-> {
                mTunerManager?.seekTuneRelease()
                mTunerManager?.seekNextAvailable(TunerManager.Direction.DOWN)

            }
            NOWPLAYING_SOURCE_TYPE.USBMSD.name -> {
                mCMediaPlayer?.previousTrack(1, 5)
            }
            NOWPLAYING_SOURCE_TYPE.AUX.name -> {

            }
        }
    }
    override fun onBrowseList() {
        val staticList = ArrayList<AMFMStationInfo_t>()
        when (mTunerManager?.waveband) {
            TunerManager.Waveband.AM -> {
                mTunerManager?.staticStationList?.forEach {
                    staticList.add(AMFMStationInfo_t(it.frequency.toFloat(),it.hdStationNameLong,it.id.toFloat(),it.hdStationNameShort,0f,eRdsStatus(), eTPStationStatus(), DataPoolDataHandler.favoriteList.contains(it.frequency.toFloat().toString())))
                }
            }
            TunerManager.Waveband.FM -> {
                mTunerManager?.staticStationList?.forEach {
                    val freq = it.frequency.toDouble() / 1000
                    staticList.add(AMFMStationInfo_t(freq.toFloat(),it.hdStationNameLong,it.id.toFloat(),it.hdStationNameShort,0f,eRdsStatus(), eTPStationStatus(), DataPoolDataHandler.favoriteList.contains(freq.toFloat().toString())))
                }
            }
            else -> return //Add other source type here when needed
        }
        SystemListener.onStaticStationList(AMFMStationList(staticList.size.toString(), staticList))
    }
    override fun onAmRequestSource() {
        if(mTunerManager!=null) {
            SystemListener.updateCarouselOnSourceChanged(NOWPLAYING_SOURCE_TYPE.AM)
            onSourceReset(NOWPLAYING_SOURCE_TYPE.AM)
            setWaveBand(TunerManager.Waveband.AM)
        }
    }

    override fun onFmRequestSource() {
        if(mTunerManager!=null) {
            SystemListener.updateCarouselOnSourceChanged(NOWPLAYING_SOURCE_TYPE.FM)
            onSourceReset(NOWPLAYING_SOURCE_TYPE.FM)
            setWaveBand(TunerManager.Waveband.FM)
        }
    }

    override fun onAMFMTuneRequest(any: Any?) {
        if (any != null) {
            val frequency = (any.toString()).toDouble()
            if (mTunerManager!!.waveband == TunerManager.Waveband.FM)
                tuneTo(( frequency* 1000).toInt())
            else
                tuneTo(frequency.toInt())
        }
    }

    override fun onFavoriteRequest(any: Any?) {
        if (any != null) {
            FavoriteDataProcessor.addOrRemoveFavorite(any as AMFMStationInfo_t)
            SystemListener.updateFavorites(any)
        }
    }

    override fun onMediaRequest() {
        onSourceReset(NOWPLAYING_SOURCE_TYPE.USBMSD)
        if (mIDevice != null) {
            if (mIDevice?.isMounted!!) {
                try {
                    mCMediaPlayer?.playDevice(mIDevice)
                }
                catch (e:ExceptionPlayFunctionNotEnabled){
                    e.printStackTrace()
                }

                return
            }
        }
    }

    override fun onAuxRequest() {
        onSourceReset(NOWPLAYING_SOURCE_TYPE.AUX)
            if (mIDevice == null) {
                /*SystemListener.onMetadataChanged(AMFMStationInfo_t(NOWPLAYING_SOURCE_TYPE.AUX,
                        metaData1 = "AUX Not Available", metaData2 = "", metaData3 = ""))*/
            }
            else{
                /*SystemListener.onMetadataChanged(AMFMStationInfo_t(NOWPLAYING_SOURCE_TYPE.AUX,
                        metaData1 = "AUX Available", metaData2 = "", metaData3 = ""))*/
            }
    }

    override fun onNowplayingPlayPause() {
        if (mCMediaPlayer?.currentPlayState == EnumPlayState.Pause ||
                mCMediaPlayer?.currentPlayState == EnumPlayState.Stop) {
            mCMediaPlayer?.play()
        }
        else {
            mCMediaPlayer?.pause()
        }
    }

    override fun onNowplayingDirectTune() {
    }
    override fun onRequestFavoriteList() {
//        var favList=ArrayList<String>()
//        favList.add("Hold to Set")
//        favList.add("Hold to Set")
//        favList.add("Hold to Set")
//        favList.add("Hold to Set")
//        favList.add("Hold to Set")
//        favList.add("Hold to Set")
//        print("FavList Size $favList.size")
//        SystemListener.onFavListChange(favList)

        FavoriteDataProcessor
    }
    override fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?) {
        println(" Source Reset!!!! ${DataPoolDataHandler.previousSourceType}")
        when(DataPoolDataHandler.previousSourceType){
            NOWPLAYING_SOURCE_TYPE.AM->{
                resetTuner()
            }
            NOWPLAYING_SOURCE_TYPE.FM->{
                resetTuner()
            }
            NOWPLAYING_SOURCE_TYPE.USBMSD,NOWPLAYING_SOURCE_TYPE.AUX->{
                resetMedia()
            }

        }
        SystemListener.onCurrentSourceChanged(sourceTYPE!!)
    }

    private fun resetMedia() {
    }

    private fun resetTuner() {
        mTunerManager?.seekTuneRelease()
        Log.v("SeekRelease!.....","resetTuner")
    }


    /*SDK Response Listener Implementation*/
    override fun onRdsStateChanged(p0: Boolean) {
        println("onRdsStateChanged")
    }

    override fun onSeekStatusChanged(p0: String?) {
        println("onSeekStatusChanged")
    }

    override fun onAutoStoreStatusChanged(p0: AutoStoreStatus?) {
        println("onAutoStoreStatusChanged")
    }

    override fun onRdsTimeChanged(p0: RdsTime?) {
        println("onRdsTimeChanged")
    }

    override fun onHdStatusInfoChanged(p0: String?) {
        println("onHdStatusInfoChanged")
    }

    override fun onAvailableStationListChanged(stationList: MutableList<TunerStation>?) {
        println("onAvailableStationListChanged")
        //processStationList(stationList)
    }

    override fun onCurrentStationChanged(stationData: TunerStation?) {
        println("onCurrentStationChanged")
        if (stationData?.getWaveband() == NOWPLAYING_SOURCE_TYPE.AM.name) {
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(AMFMStationInfo_t(stationData.frequency!!.toFloat(),stationData.hdStationNameLong,stationData.id!!.toFloat(),stationData.waveband,stationData.ptyCode!!.toFloat(),eRdsStatus(), eTPStationStatus()))
        }
        else{
            val freq = stationData!!.frequency.toDouble()/1000
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(AMFMStationInfo_t(freq.toFloat(),stationData.hdStationNameLong,stationData.id!!.toFloat(),stationData.waveband,stationData.ptyCode!!.toFloat(),eRdsStatus(), eTPStationStatus()))
        }

    }

    override fun onRadioTextPlusChanged(p0: MutableList<RadioTextPlus>?) {
        println("onRadioTextPlusChanged")
    }

    override fun onStaticStationListChanged(stationList: MutableList<TunerStation>?) {
        println("onStaticStationListChanged")
        processStationList(stationList)
    }

    override fun onFavoritesChanged(p0: MutableList<TunerFavorite>?) {
        println("onFavoritesChanged")
    }

    override fun onRadioTextChanged(p0: RadioText?) {
        println("onRadioTextChanged")
    }

    /*IDeviceListener Implementation*/
    override fun onErrorImpossible(p0: MediaRunnable?) {
    }

    override fun onErrorOperationProhibited(p0: MediaRunnable?) {
    }

    override fun onDeviceAttributeChange(iDevice: IDevice?): Boolean {
        println("onDeviceAttributeChange()->iDevice: $iDevice")
        Log.i("SDKManager", "onDeviceAttributeChange :: $iDevice")
        if (iDevice == null) {
            setIDevice(null)
            return false
        }
        setIDevice(iDevice)
        Log.i("SDKManager", "device.getObjectID() :: ${iDevice.objectID}")
        Log.i("SDKManager", "device.getType() :: ${iDevice.type}")
        Log.i("SDKManager", "device.canBrowse() :: ${iDevice.canBrowse()}")
        Log.i("SDKManager", "device.getVolumeName() :: ${iDevice.volumeName}")
        if (iDevice.type == EnumDeviceType.USBMSD) {
            SystemListener.onDeviceChanged(iDevice)
        }

        return true
    }

    override fun onErrorConnectionFailed(p0: MediaRunnable?) {
    }

    override fun onVolumeRemoved(p0: Long): Boolean {
        return false
    }

    override fun onErrorConnectionRefused(p0: MediaRunnable?) {
    }

    override fun onDeviceSyncError(iDevice: IDevice?, p1: String?): Boolean {
        println("onDeviceSyncError()->iDevice: $iDevice")
        return false
    }

    override fun onErrorFailed(p0: MediaRunnable?) {
    }

    override fun onErrorTimeout(p0: MediaRunnable?) {
    }

    override fun onErrorOperationFailed(p0: MediaRunnable?) {
    }

    override fun onDeviceAvailableChange(p0: Boolean, iDevice: IDevice?): Boolean {
        println("onDeviceAvailableChange()->iDevice: $iDevice")
        if (iDevice == null) {
            setIDevice(null)
            return false
        }
        setIDevice(iDevice)
        println("deviceType:: ${iDevice.type}")
        println("getVolumeName:: ${iDevice.volumeName}")
        Log.i("SDKManager", "device.getObjectID() :: ${iDevice.objectID}")
        Log.i("SDKManager", "device.getType() :: ${iDevice.type}")
        Log.i("SDKManager", "device.canBrowse() :: ${iDevice.canBrowse()}")
        Log.i("SDKManager", "device.getVolumeName() :: ${iDevice.volumeName}")
        if (iDevice.type == EnumDeviceType.USBMSD && iDevice.isMounted &&
                aMFMStationInfo_t.get()?.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.USBMSD.name) {
            try {
                mCMediaPlayer?.playDevice(mIDevice)
            }
            catch (e:ExceptionPlayFunctionNotEnabled){
                e.printStackTrace()
            }
        }
        else if(aMFMStationInfo_t.get()?.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.USBMSD.name) {
            /*SystemListener.onMetadataChanged(AMFMStationInfo_t(NOWPLAYING_SOURCE_TYPE.USBMSD,
                    metaData1 = "USB Not Available", metaData2 = "", metaData3 = ""))*/
        }
        return true
    }

    override fun getListAdapter(): IDeviceListAdapter? {
        return  null
    }

    override fun onErrorUnexpected(p0: MediaRunnable?) {
    }

    override fun onErrorNotConnected(p0: MediaRunnable?) {
    }

    private fun setIDevice(iDevice: IDevice?) {
        mIDevice = iDevice
    }

    /*IPlaybackListener Listener*/
    override fun onShuffleStateChange(p0: EnumShuffleState?): Boolean {
        return false
    }

    override fun onMetadataChange(iMetadata: IMetadata?): Boolean {
        if (iMetadata == null) {
            return false
        }
        //SystemListener.onMetadataChanged(AMFMStationInfo_t(enumValueOf(DataPoolDataHandler.deviceData.get()?.type!!),metaData1 = iMetadata.title, metaData2 = iMetadata.album, metaData3 = iMetadata.artist))
        return true
    }

    override fun onRepeatStateChange(p0: EnumRepeatState?): Boolean {
        return false
    }

    override fun onActiveDeviceChange(p0: Long) {
    }

    override fun onPlayStateChange(p0: EnumPlayState?): Boolean {
        return false
    }

    override fun onTrackCountChange(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun onTrackTimeChange(p0: Int, p1: Int, p2: Float): Boolean {
        return false
    }

    override fun onAlbumArtChange(p0: Bitmap?): Boolean {
        return false
    }

}
