package com.gm.audioapp.ui.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gm.audioapp.BR
import com.gm.audioapp.ui.customclasses.ManualTunerController
import com.gm.audioapp.viewmodels.AMFMStationInfo_t
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler


/**
 *
 * Created by Aswin on 3/22/2018.
 */
class CarouselRecyclerViewAdapter(var recyclerView: RecyclerView, private var mListObjects: List<*>?, private val mChildLayoutId: Int) : RecyclerView.Adapter<CarouselRecyclerViewAdapter.RecyclerViewHolder>() {
    private var mInflater: LayoutInflater? = null
    var size = 0;
    var isVisible:Boolean = false
    var isTuned:Boolean = true
    var currentVisible:Int = 0
    var isScrolling = false


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    inner class RecyclerViewHolder(// each data item is just a string in this case
            private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Any, position: Int, isVisible: Boolean) {
            binding.setVariable(BR.obj, item)
            binding.setVariable(BR.clickHandler, EventHandler)
            binding.setVariable(BR.position, position)
            binding.setVariable(BR.dataPoolHandler, DataPoolDataHandler)
            binding.setVariable(BR.isVisible, isVisible)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarouselRecyclerViewAdapter.RecyclerViewHolder {

        if (mInflater == null) {
            mInflater = parent.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        val binding = DataBindingUtil.inflate<ViewDataBinding>(mInflater!!, mChildLayoutId, parent, false)

        size = mListObjects!!.size

        return RecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CarouselRecyclerViewAdapter.RecyclerViewHolder, position: Int) {

//        var itemPosition = position//
//        if (itemPosition >= size) {//
//            val count = itemPosition / size
//            itemPosition = itemPosition - count * size//
//        }

        val item = mListObjects!![position] as AMFMStationInfo_t

        if (currentVisible == position) {
            isVisible = true
            if (!isTuned) {
                EventHandler.initEvent(ManualTunerController.eventName, item.frequency.toString())
            }
        }else isVisible = false

        holder.bind(item!!, position, isVisible)
    }

    fun updateVisibleItem(position: Int, tuned: Boolean){
        currentVisible = position
        this.isTuned = tuned
        notifyDataSetChanged()
    }

    fun updateCarouselListData(mListObjects: List<*>?){
        if (mListObjects!!.size > 0){
            this.mListObjects = mListObjects
            notifyDataSetChanged()
        }
    }

    fun updateIsScrolling(isScrolling: Boolean){
        this.isScrolling = isScrolling
    }

    override fun getItemCount(): Int {
        return mListObjects!!.size
//        return Integer.MAX_VALUE
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun setHasStableIds(hasStableIds: Boolean) {
        super.setHasStableIds(hasStableIds)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}