package com.gm.audioapp.ui.activities.fragments

import android.animation.ObjectAnimator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gm.audioapp.R
import com.gm.audioapp.databinding.NowPlayingFragmentBinding
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler

class NowPlayingFragment : Fragment(), FragmentWithFade {

    private var mRootView: View? = null
    private var mMainDisplay: View? = null
    private var fm_nowplaying_include: View? = null
    private var am_nowplaying_include: View? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: NowPlayingFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.nowplaying_layout, container, false)
        mRootView = binding.root

        binding.let {
            it.dataPoolHandler = DataPoolDataHandler
            it.clickHandler = EventHandler
        }

        am_nowplaying_include = mRootView!!.findViewById(R.id.am_nowplaying_include)
        fm_nowplaying_include = mRootView!!.findViewById(R.id.fm_nowplaying_include)
        mMainDisplay = mRootView!!.findViewById(R.id.recycler_view)

        return mRootView
    }

    override fun fadeOutContent() {
        fadeOut(mMainDisplay!!)
        fadeOut(am_nowplaying_include!!)
        fadeOut(fm_nowplaying_include!!)
    }

    fun fadeOut(view: View){
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 1f, 0f)
        containerAlphaAnimator.interpolator = NowPlayingFragment.sInterpolator
        containerAlphaAnimator.startDelay = FADE_OUT_START_DELAY_MS.toLong()
        containerAlphaAnimator.duration = FADE_ANIM_TIME_MS.toLong()
        containerAlphaAnimator.start()
    }

    override fun fadeInContent() {
        fadeIn(mMainDisplay!!)
        fadeIn(am_nowplaying_include!!)
        fadeIn(fm_nowplaying_include!!)
    }

    fun fadeIn(view: View){
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f)
        containerAlphaAnimator.interpolator = NowPlayingFragment.sInterpolator
        containerAlphaAnimator.duration = FADE_ANIM_TIME_MS.toLong()
        containerAlphaAnimator.start()
    }


    companion object {
        private val sInterpolator = FastOutSlowInInterpolator()
        private val FADE_OUT_START_DELAY_MS = 150
        private val FADE_ANIM_TIME_MS = 100
        private val fragment = NowPlayingFragment()
        fun getInstance(): NowPlayingFragment = fragment
    }

}