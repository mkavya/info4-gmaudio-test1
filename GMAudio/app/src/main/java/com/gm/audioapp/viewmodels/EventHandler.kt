package com.gm.audioapp.viewmodels

import android.view.KeyEvent
import android.util.Log
import android.view.View
import com.gm.audioapp.viewmodels.EventProcessor.eventTable
import com.gm.audioapp.viewmodels.EventProcessor.getResourceName
import com.gm.audioapp.viewmodels.EventProcessor.keyboardCommands
import com.gm.audioapp.viewmodels.EventProcessor.remoteCommands
import com.gm.audioapp.viewmodels.EventProcessor.touchViewCommands
import com.gm.audioapp.viewmodels.EventProcessor.triggerEvent
import com.gm.audioapp.viewmodels.EventProcessor.voiceCommands
import org.json.JSONObject
import android.widget.RadioGroup
import com.gm.audioapp.R.id.radioButton1
import com.gm.audioapp.R.id.radioButton2
import com.gm.audioapp.ui.activities.NowPlayingActivity


/**
 *
 * Created by GM on 3/7/2018.
 */

object EventHandler:IEventHandler{

    fun initEvent(eventName: String){
        println("initAudio:: $eventName")
        EventProcessor.processEvent(eventName, null)
    }

    fun initEvent(eventName: String, any: Any){
        println("init:: $eventName")
        EventProcessor.processEvent(eventName, any)
    }

    override fun onClickHandler(view:View){
        println("onClickHandler::$view ")
        val eventObj = eventTable[touchViewCommands[getResourceName(view.id)]] as JSONObject?
        processEventHandler(eventObj, null)
    }

    override fun onClickHandler(view:View, any: Any){
        println("onClickHandler::$view $any")
        val eventObj = eventTable[touchViewCommands[getResourceName(view.id)]] as JSONObject?
        processEventHandler(eventObj, any)
    }

    override fun onKeyHandler(keyCode: Int, event: KeyEvent?) {
       val eventObj = eventTable[keyboardCommands[event?.displayLabel.toString()]] as JSONObject?
        processEventHandler(eventObj, null)
    }

    override fun onVoiceHandler(vararg voiceControlCommands: String){
        for (command in voiceControlCommands) {
            val eventObj = eventTable[voiceCommands[command.toLowerCase()]] as JSONObject?
            if (eventObj != null) {
                processEventHandler(eventObj, null)
                break
            }
        }
    }

    override fun onRemoteHandler(remoteControlCommands: String){
            val eventObj = eventTable[remoteCommands[remoteControlCommands]] as JSONObject?
                processEventHandler(eventObj, null)
    }

    private fun processEventHandler(eventObj: JSONObject?, data: Any?) {
        if (eventObj != null) {
            val eventType = eventObj.getString("eventType")
            val eventName = eventObj.getString("eventName")
            println("processEventHandler::$eventType $eventName")
            triggerEvent(eventType, eventName, data)
        }
    }

    fun onRadioChanged(radioGroup: RadioGroup, id: Int) {
        Log.v("Pavan","view id::"+id )
        // This will get the radiobutton that has changed in its check state

        when (id) {
            radioButton1 ->  {DataPoolDataHandler.themeType.set(true)
                Log.v("Pavan","view id::radioButton1"+id )}
            radioButton2 ->  { DataPoolDataHandler.themeType.set(false)
                Log.v("Pavan","view id::radioButton2"+id )}

            }
    }

    fun onDrawerChange(position : Int,nowPlayingActivity: NowPlayingActivity){
        when(position){
            0 ->{EventProcessor.triggerEvent("process_fragment", "onAmRequestSource", null)}
            1 ->{EventProcessor.triggerEvent("process_fragment", "onFmRequestSource", null)}
            2 ->{EventProcessor.triggerEvent("navigate", "evG_Themes", null)}

        }

    }
}