package com.gm.audioapp.sdkintegration

import java.lang.reflect.InvocationTargetException

/**
 * A controller decides which platform api call must be invoked.
 *
 * Created by GM on 3/9/2018.
 */
object SystemController {

    private var manager: IManager

    /**
     * Get [IManager] w.r.t to the working platform
     */
    private fun getSourceManager(): IManager {
        val sdkManager = SDKManager()
        val androidManager = AndroidManager()
        manager = when {
            sdkManager.isTunerAvailable() -> sdkManager
            androidManager.isAndroidSDK() -> androidManager
            else -> SimulationManager()
        }
        return manager
    }

    /**
     * Trigger the produced event using obtained [IManager] object
     */

    fun execute(event: String, any: Any?) {
        val cls = IManager::class.java
        try {
            if (any != null) {
                val method = cls.getDeclaredMethod(event, Any::class.java)
                method.invoke(manager, any)
            } else {
                val method = cls.getDeclaredMethod(event)
                method.invoke(manager)
            }

        } catch (ex: InvocationTargetException) {
            var causes: Throwable = ex.cause!!
            System.err.println("An InvocationTargetException was caught!" + causes.message);

        }
    }

    init {
        manager = getSourceManager()
    }
}

