package com.gm.audioapp.sdkintegration.mock

import com.gm.audioapp.viewmodels.AMFMStationInfo_t


/**
 *
 * Created by Aswin on 3/21/2018.
 */

interface IDataSourceProvider {

    fun nowPlayingDataList(): ArrayList<AMFMStationInfo_t>
}