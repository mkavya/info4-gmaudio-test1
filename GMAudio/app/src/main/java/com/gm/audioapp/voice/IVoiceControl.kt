package com.gm.audioapp.voice;


 interface IVoiceControl {
    fun processVoiceCommands(vararg voiceControlCommands: String) // This will be executed when a voice command was found
    
    fun restartListeningService() // This will be executed after a voice command was processed to keep the recognition service activated
}
