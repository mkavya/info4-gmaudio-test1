package com.gm.audioapp.ui.activities.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gm.audioapp.R
import com.gm.audioapp.databinding.ManualTunerBinding
import com.gm.audioapp.ui.customclasses.ManualTunerController
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler

class ManualTunerFragment: Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: ManualTunerBinding = DataBindingUtil.inflate(
                inflater, R.layout.ics_audio_am_direct_tune_keypad, container, false)

        binding.let {
            it.dataPoolHandler = DataPoolDataHandler
            it.clickHandler = EventHandler
            it.manualTune = ManualTunerController
        }

        ManualTunerController.validateSourceType(DataPoolDataHandler.aMFMStationInfo_t.get()!!.rdsStationInfo)

        return binding.root
    }

}