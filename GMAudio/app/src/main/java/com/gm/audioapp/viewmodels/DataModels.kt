package com.gm.audioapp.viewmodels


/**
 *
 * Created by Aswin on 3/19/2018.
 */
data class AMFMStationList(var ucAMFMStationNum:String="",
                         var AMFMStation:List<AMFMStationInfo_t>)
//data class NowPlayingData(var sourceType:NOWPLAYING_SOURCE_TYPE=NOWPLAYING_SOURCE_TYPE.AM,var metaData1:String="",var metaData2:String="",var metaData3:String="", var isFavorite:Boolean=false)

data class DeviceData(var type: String="", var name: String="", var id: String="", var isReadable: String="", var isBrowsable: String="")

data class ManualTuneData(var frequency: String="", var isValidChannel: Boolean=false, var buttonValues: Array<Boolean> = arrayOf(true, true, true, true, true, true, true, true, true, true))

//Low radio
//## type eRdsStatus

data class eRdsStatus (
    var AMFM_RDS_NOT_AVAILABLE: Float = 0F,
    var AMFM_RDS_AVAILABLE: Float = 1F
)

//## type eTPStationStatus
data class eTPStationStatus (
    var AMFM_STATION_DOES_NOT_SUPPORT_TP: Float = 0F,
    var AMFM_STATION_SUPPORTS_TP: Float = 1F
)

data class AMFMStationInfo_t(var frequency: Float, var stationName: String, var objectId: Float, var rdsStationInfo: String, var ptyCategory: Float, var rdsStatus: eRdsStatus, var tpStationStatus: eTPStationStatus, var isFavorite:Boolean=false)

//## type AMFMTuneInfo_t

data class AMFMTuneInfo_t ( var freq: Float, var keysEnabled: Float)

//## type AMFMStationList_t
data class AMFMStationList_t (var stationlistCount: Float, var  objectList: List<AMFMStationInfo_t>)


//## type AMFMTPStatus_t
data class AMFMTPStatus_t (
    var state: eTPStationState, 		//## attribute state
    var status: Int		//## attribute status
)
//## type AMFMTrafficInfo_t
data class AMFMTrafficInfo_t (
    var name: String,		//## attribute name
    var programIdentifier: Float		//## attribute programIdentifier
)
//## type AMFMProgramTypeAlert_t
data class AMFMProgramTypeAlert_t (
    var name: String,		//## attribute name
    var content: String		//## attribute content
    )
data class FMStationAvailabilityInfo_t(
    var metadata: String,
    var stationStatus:eFMStationAvailability
)
//## type TunerCategoryList_t

data class TunerCategoryList_t (
    var categoryType: eRDSPtyNACategory,		//## attribute categoryType
    var stationlistCount: Int,		//## attribute stationlistCount
    var objectList: List<AMFMStationInfo_t>		//## attribute objectList
)