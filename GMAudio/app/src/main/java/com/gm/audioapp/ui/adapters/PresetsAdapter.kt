package com.gm.audioapp.ui.adapters

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.car.view.PagedListView
import com.gm.audioapp.viewmodels.AMFMStationInfo_t

/**
 * Adapter that will display a list of radio stations that represent the user's presets.
 */
class PresetsAdapter(private var mPresets: List<*>?, private val mChildLayoutId: Int) : RecyclerView.Adapter<PresetsViewHolder>(),PagedListView.ItemCap {

    private var mActiveNowPlayingData: Any? = null

    //private var mPresetClickListener: OnPresetItemClickListener? = null

    /**
     * Interface for a listener that will be notified when an item in the presets list has been
     * clicked.
     */
    /*interface OnPresetItemClickListener {
        *//**
         * Method called when an item in the preset list has been clicked.
         *
         * @param obj The [Any] corresponding to the clicked preset.
         *//*
        fun onPresetItemClicked(obj: Any)
    }*/

    /**
     * Set a listener to be notified whenever a preset card is pressed.
     */
    /*fun setOnPresetItemClickListener(listener: OnPresetItemClickListener) {
        mPresetClickListener = listener
    }*/

    /**
     * Indicates which radio station is the active one inside the list of presets that are set on
     * this adapter. This will cause that station to be highlighted in the list. If the station
     * passed to this method does not match any of the presets, then none will be highlighted.
     */
    fun setActiveRadioStation(station: Any) {
        mActiveNowPlayingData = station
        notifyDataSetChanged()
    }

    /* Refresh items*/
    fun updateData(mList: List<*>?) {
       mPresets = mList!!
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PresetsViewHolder {
        val mInflater = LayoutInflater.from(parent.context)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(mInflater, mChildLayoutId, parent, false)

        return PresetsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PresetsViewHolder, position: Int) {
        val station = mPresets!![position]
        val isActiveStation = (station!! as AMFMStationInfo_t).frequency.toString().equals((mActiveNowPlayingData as AMFMStationInfo_t).frequency.toString())

        holder.bindPreset(station!!, isActiveStation, itemCount, position)
    }

    /*override fun onPresetClicked(position: Int) {
        if (mPresetClickListener != null && itemCount > position) {
            mPresetClickListener!!.onPresetItemClicked(mPresets!![position]!!)
        }
    }
*/
    override fun getItemViewType(position: Int): Int {
        return PRESETS_VIEW_TYPE
    }

    override fun getItemCount(): Int {
        return if (mPresets == null) 0 else mPresets!!.size
    }

    override fun setMaxItems(max: Int) {
        // No-op. A PagedListView needs the ItemCap interface to be implemented. However, the
        // list of presets should not be limited.
    }

    companion object {
        private val PRESETS_VIEW_TYPE = 0
    }
}
