package com.gm.audioapp.navigator

import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

/**
 *
 * Created by Aswin on 3/21/2018.
 */

object ScreenMapper {

    private const val basePackage = "com.gm.audioapp"

    private const val action = "$basePackage.action"

    private const val fragment = "$basePackage.ui.activities.fragments"

    var eventTable: Map<String, String> = HashMap()
    var eventFragmentTable: Map<String, String> = HashMap()

    init {
        eventTable = getEventTable()
        eventFragmentTable = getEventFragmentTable()
    }

    private fun getEventTable(): HashMap<String, String> {
        val eventTable = HashMap<String, String>()
        val jsonEventObject = getJsonObject()
        val iterator = jsonEventObject.keys()
        iterator.forEach {
            eventTable[it] = "$action.${jsonEventObject.getString(it)}"
        }
        return eventTable
    }

    private fun getEventFragmentTable(): HashMap<String, String> {
        val eventTable = HashMap<String, String>()
        val jsonEventObject = getJsonObject()
        val iterator = jsonEventObject.keys()
        iterator.forEach {
            eventTable[it] = "$fragment.${jsonEventObject.getString(it)}"
        }
        return eventTable
    }

    private fun getJsonObject(): JSONObject {
        val stream = GMAudioApp.appContext.resources.openRawResource(R.raw.events)
        return JSONObject(getJsonString(stream)).getJSONObject("events_action_map")
    }

    private fun getJsonString(stream: InputStream): String {
        val strBuilder = StringBuilder()
        var jsonString: String? = null
        val bfReader = BufferedReader(InputStreamReader(stream,"UTF-8"))
        while ({ jsonString = bfReader.readLine(); jsonString }() != null) {
            strBuilder.append(jsonString)
        }
        return strBuilder.toString()
    }

}