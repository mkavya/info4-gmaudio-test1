package com.gm.audioapp.ui.adapters

import android.content.Context
import android.databinding.ViewDataBinding
import android.graphics.Point
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.WindowManager
import com.gm.audioapp.BR
import com.gm.audioapp.GMAudioApp

import com.gm.audioapp.R
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler


/**
 * A [RecyclerView.ViewHolder] that can bind a [AMFMStationInfo_t] to the layout
 * `R.layout.radio_preset_item`.
 */
class PresetsViewHolder
/**
 * @param presetsView A view that contains the layout `R.layout.radio_preset_item`.
 */
(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root){
    private val mPresetsCard: View = binding.root.findViewById(R.id.preset_card)

    val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    var display = windowManager.defaultDisplay
    var size = Point()


    /**
     * Interface for a listener when the View held by this ViewHolder has been clicked.
     */
    /*interface OnPresetClickListener {
        *//**
         * Method to be called when the View in this ViewHolder has been clicked.
         *
         * @param position The position of the View within the RecyclerView this ViewHolder is
         * populating.
         *//*
        fun onPresetClicked(position: Int)
    }*/

   /* init {
        mPresetsCard.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        //mPresetClickListener.onPresetClicked(adapterPosition)
    }*/

    /**
     * Binds the given [Any] to this View within this ViewHolder.
     */
    fun bindPreset(preset: Any, isActiveStation: Boolean, itemCount: Int, position: Int) {
        binding.setVariable(BR.clickHandler, EventHandler)
        binding.setVariable(BR.dataPoolHandler, DataPoolDataHandler)
        binding.setVariable(BR.obj, preset)
        binding.setVariable(BR.position, position)
        binding.setVariable(BR.isactive, isActiveStation)
        setPresetCardBackground(itemCount)
    }

    /**
     * Sets the appropriate background on the card containing the preset information. The cards
     * need to have rounded corners depending on its position in the list and the number of items
     * in the list.
     */
    private fun setPresetCardBackground(itemCount: Int) {
        val position = adapterPosition

        // Correctly set the background for each card. Only the top and last card should
        // have rounded corners.
        display.getSize(size)
        var mScreenWidth = size.x
        var width = mScreenWidth/2
        mPresetsCard.layoutParams.width = width
//        mPresetsCard.layoutParams.width = GMAudioApp.appContext.resources.getInteger(R.integer.preset_item_width)
        when {
            itemCount == 1 -> // One card - all corners are rounded
                mPresetsCard.setBackgroundResource(R.drawable.preset_item_card_rounded_bg)
            position == 0 -> // First card gets rounded top
                mPresetsCard.setBackgroundResource(R.drawable.preset_item_card_rounded_top_bg)
            position == itemCount - 1 -> // Last one has a rounded bottom
                mPresetsCard.setBackgroundResource(R.drawable.preset_item_card_rounded_bottom_bg)
            else -> // Middle have no rounded corners
            {
                //mPresetsCard.setBackgroundResource(R.color.car_card)
                //ManA
                val a = GMAudioApp.appContext.obtainStyledAttributes(intArrayOf(com.android.gm.ui.R.attr.gm_car_card_color))
                var cardColor = GMAudioApp.appContext.getColor(R.color.car_card_light)
                try {
                    cardColor = a.getColor(com.android.gm.ui.R.styleable.GMBackgroundColorTheme_gm_car_card_color, GMAudioApp.appContext.getColor(R.color.car_card_light))
                    if (GMAudioApp.useCadillacTheme!!) {
                        cardColor = GMAudioApp.appContext.getColor(R.color.car_card_light)
                    } else {
                        cardColor = GMAudioApp.appContext.getColor(R.color.car_card_dark)
                    }
                } finally {
                    a.recycle()
                }
                mPresetsCard.setBackgroundColor(cardColor)
            }
        }
    }

}
