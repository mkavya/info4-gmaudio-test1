package com.gm.audioapp.ui.adapters

import android.animation.ValueAnimator
import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.graphics.Rect
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.util.Log
import android.widget.TextView
import com.android.car.view.PagedListView
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.ui.customclasses.StartSnapHelper
import com.gm.audioapp.viewmodels.AMFMStationInfo_t
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.DataPoolDataHandler.carouselRecyclerViewAdapter
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE
import java.text.DecimalFormat


/**
 *
 * Created by Aswin on 3/16/2018.
 */


@BindingAdapter("bind:items", "bind:childlayout")
fun bindList(recyclerView: RecyclerView, items: ObservableArrayList<*>, childlayout: Int) {
    val layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.layoutManager = layoutManager
    Log.d("bindList", "list size = " + items.size)

    if (childlayout == R.layout.uil_audio_row_list_am_carousel_list_view) {
        isPaddingAlreadySet = false
        carouselRecyclerViewAdapter = CarouselRecyclerViewAdapter(recyclerView, items, childlayout)
        recyclerView.adapter = carouselRecyclerViewAdapter
        carouselRecyclerViewAdapter?.notifyDataSetChanged()

        val startSnapHelper = StartSnapHelper()
        recyclerView.setOnFlingListener(null);
        startSnapHelper.attachToRecyclerView(recyclerView)

        for (position in (items as ArrayList<AMFMStationInfo_t>).indices) {
            val frequency :String
            if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!! == NOWPLAYING_SOURCE_TYPE.AM)
                frequency = DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString()
            else
                frequency = DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString()

            if (frequency.equals(items[position].frequency.toString())) {
                layoutManager.scrollToPositionWithOffset(position, 0)
                carouselRecyclerViewAdapter?.updateVisibleItem(position, true)
                break
            }
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var isScrollStareDragging = false

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (!isPaddingAlreadySet)
                    setRecyclerViewPadding(layoutManager, recyclerView)

                if (isScrollStareDragging) {
                    updateCarouselView(layoutManager)
                }

                val firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition()
                if (firstVisiblePosition == 0) {
                    isScrollStareDragging = false
                    carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    isScrollStareDragging = false
                    updateCarouselView(layoutManager)
                    carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)

                }

                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isScrollStareDragging = true
                    carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)
                }
            }
        })

    } else {
        val adapter = RecyclerViewAdapter(items, childlayout)
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }
}

fun updateCarouselView(layoutManager: LinearLayoutManager) {
    val position = layoutManager.findFirstCompletelyVisibleItemPosition()
    if (position != -1) {
        carouselRecyclerViewAdapter?.updateVisibleItem(position, false)
    }
}

var isPaddingAlreadySet = false
fun setRecyclerViewPadding(layoutManager: LinearLayoutManager, recyclerView: RecyclerView) {
    val firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition()
    if (firstVisiblePosition != -1) {

        val rvRect = Rect()
        recyclerView.getGlobalVisibleRect(rvRect)

        val view = layoutManager.findViewByPosition(firstVisiblePosition)
        if (view != null) {
            val height = view.getMeasuredHeight()
            var totalHeight: Int

            isPaddingAlreadySet = true
//            totalHeight = rvRect.bottom - height - height / 6
            var dis = DisplayMetrics()
            if (dis.heightPixels > 1000)
                totalHeight = rvRect.bottom - GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.ics_audio_am_station_list_CardView12_height) - height
            else  totalHeight = rvRect.bottom - GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.ics_audio_am_station_list_CardView12_height) - height/2
            recyclerView.setPadding(0, 0, 0, totalHeight)
        }
    }
}

/*@BindingAdapter("bind:items")
fun bindList(pager: ViewPager, items: ObservableArrayList<String>) {
    Log.d("bindList ViewPager ", "bindViewpager : ")
    pager.adapter = ViewPagerAdapter((pager.context as FragmentActivity).supportFragmentManager, items)
}*/

/*
@BindingAdapter("bind:viewPager")
fun bindList(tabLayout: TabLayout, viewPager: Int) {
    Log.d("bindList ViewPager ", "bind:viewTab : ")
    tabLayout.setupWithViewPager(NowPlayingActivity.viewPagerOBJ as ViewPager)
    tabLayout.tabGravity = TabLayout.GRAVITY_CENTER
    tabLayout.tabMode = TabLayout.MODE_SCROLLABLE

}
*/

var presetsAdapter: PresetsAdapter? = null

@BindingAdapter("bind:items", "bind:childlayout")
fun bindList(pagedListView: PagedListView, items: ObservableArrayList<*>, childLayout: Int) {
    if (presetsAdapter != null && presetsAdapter!!.itemCount > 0) {
        presetsAdapter!!.updateData(items)
    } else {
        presetsAdapter = PresetsAdapter(items, childLayout)
    }

    if (pagedListView.adapter == null || pagedListView.itemCount != presetsAdapter!!.itemCount)
        pagedListView.setAdapter(presetsAdapter!!)

    presetsAdapter!!.setActiveRadioStation(DataPoolDataHandler.aMFMStationInfo_t.get()!! as Any)
    pagedListView.setLightMode()
    pagedListView.layoutManager.setOffsetRows(false)
}

/*@BindingAdapter("bind:items"*//*,"bind:nowPlaying"*//*)
fun bindList(carouselView: CarouselView*//*, aMFMStationInfo_t: ObservableField<AMFMStationInfo_t>*//*, items:ObservableArrayList<AMFMStationInfo_t>) {
    val adapter = PrescannedRadioStationAdapter()
    adapter.setStations(items)
    carouselView.setAdapter(adapter)
    adapter.notifyDataSetChanged()
    *//*if (aMFMStationInfo_t != null) {
        adapter.setStartingStation(aMFMStationInfo_t.get()?.metaData1!!, aMFMStationInfo_t.get()?.sourceType!!)
        carouselView.shiftToPosition(adapter.currentPosition)
    }*//*
}*/

@BindingAdapter("bind:color")
fun textColor(textView: TextView, color: Int) {
    textView.setTextColor(color)
}


@BindingAdapter("bind:text")
fun setText(textView: TextView, aMFMStationInfo_t: AMFMStationInfo_t) {
    if (aMFMStationInfo_t.rdsStationInfo.equals(NOWPLAYING_SOURCE_TYPE.AM.name))
        textView.text = aMFMStationInfo_t.frequency.toString().split(".")[0]
    else
        textView.text = aMFMStationInfo_t.frequency.toString()

}
var previousFrequency = "0.0f"
@BindingAdapter("bind:text")
fun setText(textView: TextView, frequency: String) {

    if (DataPoolDataHandler.aMFMStationInfo_t.get()!!.rdsStationInfo.equals(NOWPLAYING_SOURCE_TYPE.AM.name)) {

        val mAmAnimatorListener = ValueAnimator.AnimatorUpdateListener { animation ->

            textView.text = AM_FORMATTER.format(animation.animatedValue)
        }
        animateRadioChannelChange(previousFrequency.toFloat(), frequency.toFloat(), mAmAnimatorListener)
    }else{
        val mFmAnimatorListener = ValueAnimator.AnimatorUpdateListener { animation ->

            textView.text = FM_FORMATTER.format(animation.animatedValue)
        }
        animateRadioChannelChange(previousFrequency.toFloat(), frequency.toFloat(), mFmAnimatorListener)
    }

    previousFrequency = frequency
}

val FM_CHANNEL_FORMAT = "###.#"
val AM_CHANNEL_FORMAT = "####"

/**
 * The formatter for AM radio stations.
 */
val FM_FORMATTER = DecimalFormat(FM_CHANNEL_FORMAT)

/**
 * The formatter for FM radio stations.
 */
val AM_FORMATTER = DecimalFormat(AM_CHANNEL_FORMAT)


private fun animateRadioChannelChange(startValue: Float, endValue: Float,
                                      listener: ValueAnimator.AnimatorUpdateListener) {

    val CHANNEL_CHANGE_DURATION_MS = 200

    val animator = ValueAnimator()
    animator.setObjectValues(startValue, endValue)
    animator.duration = CHANNEL_CHANGE_DURATION_MS.toLong()
    animator.addUpdateListener(listener)
    animator.start()
}

