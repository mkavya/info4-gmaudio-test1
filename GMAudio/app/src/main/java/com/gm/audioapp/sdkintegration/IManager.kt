package com.gm.audioapp.sdkintegration

import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE

/**
 * API calls for three platforms: GM SDK, Android SDK and Simulation.
 *
 * Created by Aswin on 3/14/2018.
 */
interface IManager {

    /**
     * Initialize all prerequisites for all source types
     */
    fun initAudio()

    /**
     * To get the list of favourite list set by end user
     */
    fun onRequestFavoriteList()

    /**
     * Triggers on click event of next button
     */
    fun onRequestNowPlayingNext()

    /**
     * Triggers on click event of previous button
     */
    fun onRequestNowPlayingPrevious()

    /**
     * Triggers on selection of AM station
     */
    fun onAmRequestSource()

    /**
     * Triggers on selection of FM station
     */
    fun onFmRequestSource()

    /**
     * Triggers on selection of USB source
     */
    fun onMediaRequest()

    /**
     * Triggers on selection of AUX source
     */
    fun onAuxRequest()

    /**
     * Calls to show a list of AM/FM Stations and any media list
     */
    fun onBrowseList()

    /**
     * Triggers on click for play/pause
     */
    fun onNowplayingPlayPause()

    /**
     * Triggers for manual radio tuner
     */
    fun onNowplayingDirectTune()

    /**
     * Set the current selected source type.
     *
     * @param sourceTYPE Audio source type
     */
    fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?)

    /**
     * Set the selected channel
     */
    fun onAMFMTuneRequest(any: Any?)

    /**
     * Set the selected to Favorite
     */
    fun onFavoriteRequest(any: Any?)
}