package com.gm.audioapp.navigator

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import com.gm.audioapp.GMAudioApp.Companion.activityContext
import com.gm.audioapp.GMAudioApp.Companion.activityContext1
import com.gm.audioapp.R
import com.gm.audioapp.ui.activities.BaseActivity
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.audioapp.ui.activities.NowPlayingActivity.Companion.CONTENT_FRAGMENT_TAG
import com.gm.audioapp.ui.activities.NowPlayingActivity.Companion.MANUAL_TUNER_BACKSTACK
import com.gm.audioapp.ui.activities.fragments.FragmentWithFade
import com.gm.audioapp.ui.activities.fragments.ManualTunerFragment
import com.gm.audioapp.ui.activities.fragments.NowPlayingFragment

open class ActivityNavigator : Navigator {

    fun triggerActivity(eventName: String, any: Any?) {
        startActivity(ScreenMapper.eventTable[eventName]!!, Bundle())
    }

    fun triggerFragment(eventName: String, any: Any?){
        //val fragment: Fragment = Fragment.instantiate(activityContext, ScreenMapper.eventFragmentTable[eventName])
        val fragmentName = ScreenMapper.eventFragmentTable[eventName]
        val fragment: Fragment = when(eventName) {
            "onAmRequestSource", "onFmRequestSource" -> {
                /*(activityContext as NowPlayingActivity).supportFragmentManager.findFragmentByTag(fragmentName)
                        ?:*/ NowPlayingFragment.getInstance()
            }
            else -> Fragment.instantiate(activityContext, fragmentName)
        }
        replaceFragment((activityContext as NowPlayingActivity).getContainerId(), fragment, Bundle())
    }

    override fun finishActivity() {
        activityContext.finish()
        activityContext1.recreate()
    }

    override fun startActivity(intent: Intent) {
        activityContext.startActivity(intent)
    }

    override fun startActivity(action: String) {
        val intent = Intent(action)
        startActivity(intent)
    }

    override fun startActivity(action: String, uri: Uri) {}

    override fun startActivity(action: String, args: Bundle) {
        val intent = Intent(action)
        intent.putExtras(args)
        startActivity(intent)
    }

    override fun startActivity(activityClass: Class<out BaseActivity>) {
        startActivity(Intent(activityContext, activityClass))
    }

    override fun startActivity(activityClass: Class<out BaseActivity>, args: Bundle) {}

    override fun startActivity(activityClass: Class<out BaseActivity>, args: Parcelable) {}

    override fun startActivity(activityClass: Class<out BaseActivity>, arg: String) {}

    override fun startActivity(activityClass: Class<out BaseActivity>, arg: Int) {}

    override fun startActivityForResult(activityClass: Class<out BaseActivity>, requestCode: Int) {}


    override fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: Parcelable, requestCode: Int) {}

    override fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: String, requestCode: Int) {}

    override fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: Int, requestCode: Int) {}

    override fun replaceFragment(@IdRes containerId: Int, fragment: Fragment, args: Bundle) {

        val supportFragmentManager = (activityContext as NowPlayingActivity).supportFragmentManager
        val currentFragment = (activityContext as NowPlayingActivity).getCurrentFragment()

        if (supportFragmentManager.backStackEntryCount > 0) {
            // A station can only be selected if the manual tuner fragment has been shown; so, remove
            // that here.
            supportFragmentManager.popBackStack()

            if (currentFragment is FragmentWithFade) {
                (currentFragment as FragmentWithFade).fadeInContent()
            }
        }else {

            if (currentFragment is FragmentWithFade && fragment !is NowPlayingFragment) {
                (currentFragment as FragmentWithFade).fadeOutContent()
            }

            if (fragment is ManualTunerFragment) {

                supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.slide_up, R.anim.slide_down,
                                R.anim.slide_up, R.anim.slide_down)
                        .add(containerId, fragment)
                        .addToBackStack(MANUAL_TUNER_BACKSTACK)
                        .commit()
            }else{
                supportFragmentManager.beginTransaction()
                        .replace(containerId, fragment, CONTENT_FRAGMENT_TAG)
                        .commitNow()
            }
        }
    }

    override fun replaceFragment(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle) {}

    override fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, args: Bundle, backstackTag: String) {}

    override fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle, backstackTag: String) {}

}
