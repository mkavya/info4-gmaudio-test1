package com.gm.audioapp.ui.customclasses

import android.content.ContentValues
import android.database.Cursor
import android.os.HandlerThread
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.util.Log


import com.gm.audioapp.GMAudioApp

import gm.entertainment.FavoriteData
import gm.provider.GMFavoritesContract

import java.util.ArrayList

import java.util.HashMap

import android.content.ContentValues.TAG
import com.gm.audioapp.viewmodels.AMFMStationInfo_t
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE

object FavoriteDataProcessor : Loader.OnLoadCompleteListener<Cursor> {

    private val mUserSelectedFavPageData = HashMap<Int, ArrayList<FavoriteData>>()
    private val mLockFavoritePageData = Any()

    private val mFavoritePageData = HashMap<Int, ArrayList<FavoriteData>>()

    private val FAVORITE_PAGE_ITEM_COUNT = 5
    private val FAVORITE_MAX_PAGE = 8

    val HD_STATION = "HD"
    val NO_INDEX_FOR_FAVORITE = -1
    val EXTRA_SPLITTER = "|"
    val FAVORITE_BY_STAR_ICON = -1

    init {
        lazyInit()
    }


    fun lazyInit() {
        val mCursorLoaderThread = object : HandlerThread("FavCursorLoaderThread") {
            override fun onLooperPrepared() {
                initLoaderForFavoritesDataUpdation()
            }
        }
        mCursorLoaderThread.start()
    }

    /**
     * This method is used to intialise the loader and listen to the favorites
     * content provider for the updates while insertion, deletion of the
     * favorites.
     */
    private fun initLoaderForFavoritesDataUpdation() {
        val selection = (GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_ALBUM + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_ARTIST + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_GENRE + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_COMPILATION + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_COMPOSER + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_PLAYLIST + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_PODCAST + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_MEDIA_VIDEO + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_FM_FREQUENCY + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_AM_FREQUENCY + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_DAB_STATION + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_SDARS_CHANNEL + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_PANDORA_STATION + " OR "
                + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                + GMFavoritesContract.FAV_TYPE.FT_APP_STATION)

        val mCursorLoader = CursorLoader(GMAudioApp.appContext, GMFavoritesContract.Favorite.CONTENT_URI, null,
                selection, null, GMFavoritesContract.Favorite.Cols.FAVORDER + " ASC ")

        mCursorLoader.registerListener(1, this)
        mCursorLoader.startLoading()
    }


    override fun onLoadComplete(loader: Loader<Cursor>, cursor: Cursor?) {
        updateFavoriteBar(cursor)
        loader.unregisterListener(this)
    }

    private fun updateFavoriteBar(cursor: Cursor?) {

        synchronized(mLockFavoritePageData) {
            mUserSelectedFavPageData.clear()
            var curPage = -1

            if (cursor != null && cursor.count > 0) {
                var favOrder = -1
                var curIndex = -1

                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    favOrder = Integer.parseInt(cursor.getString(
                            cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVORDER)))
                    curPage = favOrder / FAVORITE_PAGE_ITEM_COUNT
                    Log.d(TAG, "updateFavoriteBar curPage=$curPage favOrder=$favOrder")

                    curIndex = favOrder % FAVORITE_PAGE_ITEM_COUNT
                    var favDataList: ArrayList<FavoriteData>? = null
                    while (curPage >= mFavoritePageData.size - 1 && mFavoritePageData
                                    .size < FAVORITE_MAX_PAGE) {// Don't
                        // add
                        // max
                        // page
                        // +1.
                        Log.d(TAG, "updateFavoriteBar: add a new page   mFavoritePageArray.size()=" + mFavoritePageData.size)
                        favDataList = constructEmptyData(mFavoritePageData.size)
                        mFavoritePageData[mFavoritePageData.size] = favDataList
                    }
                    favDataList = mFavoritePageData[curPage]
                    if (favDataList != null) {
                        val curFavData = favDataList[curIndex]
                        setFavoriteItemData(cursor, curFavData)
                    }
                    cursor.moveToNext()
                }
            } else {
                // Empty page at the end..
                val favDataList = constructEmptyData(0)
                mFavoritePageData[0] = favDataList
            }

            var favListData = ArrayList<String>()
            var browseListData = DataPoolDataHandler.browseList.toList()
            for (i in 0 until mFavoritePageData.size) {
                val favList = mFavoritePageData[i]
                Log.d(TAG,
                        " data list " + mFavoritePageData.size + " favList " + favList.toString())
                for (j in 0 until favList!!.size) {
                    favListData.add(favList[j].favLableText)


                        DataPoolDataHandler.browseList.forEachIndexed({index, nowPlayingData->
                            kotlin.run {
                                val favorite = nowPlayingData.frequency.toString().equals(favList[j].favLableText)
                                if (favorite) {
                                    nowPlayingData.isFavorite = favorite
                                    DataPoolDataHandler.browseList[index] = nowPlayingData
                                }
                            }
                        })
                }
            }

            DataPoolDataHandler.favoriteList.clear()
            DataPoolDataHandler.favoriteList.addAll(favListData)


            var data = DataPoolDataHandler.aMFMStationInfo_t.get()
            var nowPlayingData = AMFMStationInfo_t(data!!.frequency, data.stationName, data.objectId, data.rdsStationInfo, data.ptyCategory, data.rdsStatus, data.tpStationStatus)
            nowPlayingData!!.isFavorite = DataPoolDataHandler.favoriteList.contains(nowPlayingData!!.frequency.toString())
            DataPoolDataHandler.aMFMStationInfo_t.set(nowPlayingData)

        }
    }

    private fun constructEmptyData(pageNumber: Int): ArrayList<FavoriteData> {
        // TODO : Don't simply create this objects. Implement
        // ArrayList<FavoriteData> object recycler pool.
        val lFavoriteDataList = ArrayList<FavoriteData>()

        for (i in 0 until FAVORITE_PAGE_ITEM_COUNT) {
            val favData = FavoriteData()
            favData.favLableText = "Hold to set"
            val index = FAVORITE_PAGE_ITEM_COUNT * pageNumber + i
            favData.position = index
            lFavoriteDataList.add(favData)
        }
        return lFavoriteDataList
    }

    private fun setFavoriteItemData(cursor: Cursor?, favData: FavoriteData?) {
        Log.d(TAG, "setFavoriteItemData")
        if (cursor != null && favData != null) {
            favData.id = cursor.getInt(cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.ID))
            favData.favType = cursor
                    .getString(cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVTYPE))
            favData.favLableText = cursor.getString(
                    cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVLABELTEXT))
            favData.favDescText = cursor.getString(
                    cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVDESCTEXT))
            favData.favPackage = cursor.getString(
                    cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVPACKAGE))
            favData.favClass = cursor
                    .getString(cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVCLASS))
            favData.favAction = cursor
                    .getString(cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVACTION))
            favData.favExtraData = cursor.getString(
                    cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA))
            favData.favOrder = cursor
                    .getString(cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVORDER))
            Log.d(TAG, "setFavoriteItemData: favOrder=" + cursor
                    .getString(cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVORDER)))
            Log.d(TAG,
                    " setFavoriteItemData : getFavLableText() - " + favData.favLableText
                            + ": getFavOrder() - " + favData.favOrder + " : isActive() -"
                            + favData.isActive)

        }
    }

    fun addOrRemoveFavorite(stationInfo: AMFMStationInfo_t?){
        if (stationInfo!!.isFavorite) {
            removeFavorite(stationInfo)
        } else {
            addFavorite(stationInfo)
        }

    }

    fun addFavorite(stationInfo: AMFMStationInfo_t?){


            var values: ContentValues? = null


            if (null != stationInfo) {
                val source = stationInfo!!.rdsStationInfo

                values = ContentValues()
                if (source != null) {
                    val favType = getFavTypeFromAudioSource(source!!)
                    values = getContentValueFromFavoriteStation(favType, FAVORITE_BY_STAR_ICON,
                            source, stationInfo)

                } else {
                }

            } else {
            }

            if (null != values && values.size() > 0) try {

                val uri = GMAudioApp.appContext.getContentResolver()!!.insert(GMFavoritesContract.Favorite.CONTENT_URI, values)

            } catch (e: Exception) {
            }
    }

    private fun getFavTypeFromAudioSource(audioSources: String): Int {
        var lFavType = GMFavoritesContract.FAV_TYPE.FT_UKNOWN
        when (audioSources) {
            NOWPLAYING_SOURCE_TYPE.AM.name -> lFavType = GMFavoritesContract.FAV_TYPE.FT_AM_FREQUENCY
            NOWPLAYING_SOURCE_TYPE.FM.name -> lFavType = GMFavoritesContract.FAV_TYPE.FT_FM_FREQUENCY
            else -> {
            }
        }
        return lFavType
    }

    private fun getContentValueFromFavoriteStation(favType: Int, position: Int, source: String, stationInfo: AMFMStationInfo_t?): ContentValues {
        val lContentValues = ContentValues()
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVTYPE, favType)
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVCLASS, "")
        var isFavPackageSet = false
        if (!isFavPackageSet) {
            lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVPACKAGE, source)
        }

        if (position >= 0) {
            lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVORDER, position)
        }

        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVLABELTEXT,
                stationInfo!!.frequency.toString())
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVDESCTEXT,
                stationInfo!!.frequency.toString())

        return lContentValues
    }

    fun removeFavorite(stationInfo: AMFMStationInfo_t?) {

        val position = FAVORITE_BY_STAR_ICON
        Log.d(TAG, "removeFavorite Station=" + stationInfo!!)
        if (null == stationInfo)
            return

        val source = stationInfo.rdsStationInfo
        var selection: String? = null
        var SelectionArgs = arrayOf(source, source + HD_STATION, "", "")
        var lastIndex = 3

        if (position > NO_INDEX_FOR_FAVORITE) {
            selection = ("((" + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?" + " OR "
                    + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?)" + " AND "
                    + GMFavoritesContract.Favorite.Cols.FAVORDER + "= ?" + " AND "
                    + GMFavoritesContract.Favorite.Cols.FAVTYPE + "= ?)")
            SelectionArgs[2] = position.toString()
        } else if (source == NOWPLAYING_SOURCE_TYPE.FM.name) {
            // This is to fix Elvis 1985389: Error when trying to delete a
            // previous saved FM
            // When tuner antenna is disconnected & FM favorite is saved from
            // Now playing screen, FreqDesc is saved with '0' picode (ex., 91.1|0 ).
            // so Additional SelectionArgs column is added here,
            // to check for both if freqdesc contains 0 picode (ex: 91.1|0 ) or valid
            // picode (91.1|picode_number)
            SelectionArgs = arrayOf(source, source + HD_STATION, "", "", "")
            selection = ("((" + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?" + " OR "
                    + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?)" + " AND ("
                    + GMFavoritesContract.Favorite.Cols.FAVDESCTEXT + " LIKE ?" + " OR "
                    + GMFavoritesContract.Favorite.Cols.FAVDESCTEXT + " LIKE ?" + ") AND "
                    + GMFavoritesContract.Favorite.Cols.FAVTYPE + "= ?)")
            SelectionArgs[2] = stationInfo.frequency.toString() + "%"
            SelectionArgs[3] = stationInfo.frequency.toString() + EXTRA_SPLITTER + "0" + "%"
//            if (AudioApp.DEBUG) {
//                Log.d(TAG, "FM AudioSource, Favorite Description SelectionArgs=" + SelectionArgs[3])
//            }
            lastIndex = 4
        } else {
            selection = ("((" + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?" + " OR "
                    + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?)" + " AND "
                    + GMFavoritesContract.Favorite.Cols.FAVDESCTEXT + " LIKE ?" + " AND "
                    + GMFavoritesContract.Favorite.Cols.FAVTYPE + "= ?)")

            when (source) {
                NOWPLAYING_SOURCE_TYPE.AM.name -> SelectionArgs[2] = stationInfo.frequency.toString() + "%"
                NOWPLAYING_SOURCE_TYPE.FM.name -> SelectionArgs[2] = stationInfo.frequency.toString() + "%"
                //NOWPLAYING_SOURCE_TYPE.DAB ->
                    //this is DAB's description text, it is not used for display,
                    //but used for comparison for current highlight.
//                    SelectionArgs[2] = UIFavoriteUtils.getDABFavDescText(stationInfo)/
            }
        }

        when (source) {
            NOWPLAYING_SOURCE_TYPE.AM.name -> SelectionArgs[lastIndex] = GMFavoritesContract.FAV_TYPE.FT_AM_FREQUENCY.toString()
            NOWPLAYING_SOURCE_TYPE.FM.name -> SelectionArgs[lastIndex] = GMFavoritesContract.FAV_TYPE.FT_FM_FREQUENCY.toString()
            //NOWPLAYING_SOURCE_TYPE.DAB -> SelectionArgs[lastIndex] = GMFavoritesContract.FAV_TYPE.FT_DAB_STATION.toString()
        }

        var count = 0

        if (null != selection) {
            count = GMAudioApp.appContext.getContentResolver()!!.delete(GMFavoritesContract.Favorite.CONTENT_URI,
                    selection, SelectionArgs)

        }
        Log.d(TAG, "removeFavorite count = $count")


    }

}
