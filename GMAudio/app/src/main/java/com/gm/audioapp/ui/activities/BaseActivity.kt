package com.gm.audioapp.ui.activities

import android.content.pm.PackageManager
import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.viewmodels.EventHandler
import android.util.Log
import android.view.Window
import android.view.WindowManager

import com.gm.audioapp.GMAudioApp.Companion.useCadillacTheme
import com.gm.audioapp.R
import com.gm.audioapp.navigator.ActivityNavigator
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.audioapp.viewmodels.DataPoolDataHandler

/**
 *
 * Created by Aswin on 3/12/2018.
 */
abstract class BaseActivity: AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        EventHandler.onKeyHandler(keyCode,event)
        return super.onKeyUp(keyCode, event)
    }

    override fun getTheme(): Resources.Theme {
        val theme = super.getTheme()
               Log.d("GMDrawerActivity::", "Manish getTheme() Cadillac/NonCadillac useCadillacTheme=" + useCadillacTheme);

        Log.v("Pavan","view id:: value:: "+DataPoolDataHandler.themeType )


        useCadillacTheme=DataPoolDataHandler.themeType.get()
        if (useCadillacTheme!!) {
            Log.v("Pavan","view id::radioButton1 in if "+useCadillacTheme )
            theme.applyStyle(R.style.CadillacTheme, true)
        }else{
            Log.v("Pavan","view id::radioButton2 in else"+useCadillacTheme )
            theme.applyStyle(R.style.NonCadillacTheme, true)
        }
        // you could also use a switch if you have many themes that could apply
        return theme
    }
}