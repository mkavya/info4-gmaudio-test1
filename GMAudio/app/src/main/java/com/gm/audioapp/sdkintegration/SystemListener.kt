package com.gm.audioapp.sdkintegration

import android.support.v7.widget.LinearLayoutManager
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.ui.customclasses.ManualTunerController
import com.gm.audioapp.ui.customclasses.RadioChannelColorMapper
import com.gm.audioapp.viewmodels.*
import com.gm.audioapp.viewmodels.DataPoolDataHandler.aMFMStationInfo_t
import com.gm.audioapp.viewmodels.DataPoolDataHandler.browseList
import com.gm.audioapp.viewmodels.DataPoolDataHandler.browseListAM
import com.gm.audioapp.viewmodels.DataPoolDataHandler.browseListFM
import com.gm.audioapp.viewmodels.DataPoolDataHandler.carouselRecyclerViewAdapter
import com.gm.audioapp.viewmodels.DataPoolDataHandler.deviceData
import com.gm.audioapp.viewmodels.DataPoolDataHandler.favoriteList
import com.gm.audioapp.viewmodels.DataPoolDataHandler.previousSourceType
import gm.media.interfaces.IDevice

/**
 * Simply updates [DataPoolDataHandler] objects
 *
 * Created by GM on 3/9/2018.
 */
object SystemListener {

    fun onCurrentSourceChanged(waveBand:NOWPLAYING_SOURCE_TYPE) {
        println("onCurrentSourceChanged")
        previousSourceType=waveBand
        ManualTunerController.validateSourceType(waveBand.name)
        DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.set(waveBand)
    }

    fun updateCarouselOnSourceChanged(waveBand:NOWPLAYING_SOURCE_TYPE){

        if (waveBand == NOWPLAYING_SOURCE_TYPE.AM && !browseListAM.isEmpty()) {
            browseList.clear()
            browseList.addAll(browseListAM)
            SystemListener.updateCarouselView(DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString(), waveBand)

        }else if (waveBand == NOWPLAYING_SOURCE_TYPE.FM && !browseListFM.isEmpty()){
            browseList.clear()
            browseList.addAll(browseListFM)
            SystemListener.updateCarouselView(DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString(), waveBand)
        }

    }

    fun onStaticStationList(amFMStationList: AMFMStationList) {
        println("onStaticStationList")
        browseList.clear()
        browseList.addAll(amFMStationList.AMFMStation)

        val sourceType = DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!!

        if (sourceType == NOWPLAYING_SOURCE_TYPE.AM) {
            browseListAM.clear()
            browseListAM.addAll(amFMStationList.AMFMStation)
        }else if (sourceType == NOWPLAYING_SOURCE_TYPE.FM){
            browseListFM.clear()
            browseListFM.addAll(amFMStationList.AMFMStation)
        }
    }


    fun onAMFM_RES_FMCATEGORYSTATIONLIST(tunercategorylist : TunerCategoryList_t) {
        DataPoolDataHandler.FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE.set(tunercategorylist.categoryType.value.toFloat());
        DataPoolDataHandler.FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT.set(tunercategorylist.stationlistCount);

        println("onAMFM_RES_FMCATEGORYSTATIONLIST")

    }

    fun onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo : AMFMStationInfo_t) {

        //Setting values for UI
//        var stationData: AMFMStationInfo_t=AMFMStationInfo_t()
//        stationData.sourceType=NOWPLAYING_SOURCE_TYPE.AM
//        stationData.metaData1=amfmstationinfo.frequency.toInt().toString()
//        stationData.metaData3=amfmstationinfo.stationName
//        stationData.metaData2=amfmstationinfo.rdsStationInfo
//        stationData.isFavorite= if(favoriteList.contains(stationData.metaData1))true else false


        aMFMStationInfo_t.set(AMFMStationInfo_t(amfmstationinfo.frequency,amfmstationinfo.stationName,0f,amfmstationinfo.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus(), if(favoriteList.contains(amfmstationinfo.frequency.toString()))true else false))

        DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.set(amfmstationinfo.frequency.toInt().toString())
        DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_STATIONNAME.set(amfmstationinfo.stationName)
        DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO.set(amfmstationinfo.rdsStationInfo)
        DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_OBJECTID.set(amfmstationinfo.objectId);
        DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_RDSSTATUS.set(amfmstationinfo.rdsStatus.AMFM_RDS_AVAILABLE);
        DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY.set(0F);
        DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS.set(amfmstationinfo.tpStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP);

        SystemListener.updateCarouselView(DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString(), NOWPLAYING_SOURCE_TYPE.AM)
        //changeBackground(NOWPLAYING_SOURCE_TYPE.AM.value.toInt(),DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat()) TODO

        updateRadioPresetsList(aMFMStationInfo_t.get()!!)

        //SystemListener.updateCarouselView(stationData.metaData1, NOWPLAYING_SOURCE_TYPE.AM)
        //changeBackground(stationData.sourceType.value.toInt(),(stationData.metaData1).toFloat())
       // changeBackground(0,amfmstationinfo.frequency)



    }
    fun onAMFM_RES_FMCURRENTSTATIONINFO(amfmstationinfo : AMFMStationInfo_t) {

        //Setting values for UI
//        var stationData: AMFMStationInfo_t=AMFMStationInfo_t()
//        stationData.sourceType=NOWPLAYING_SOURCE_TYPE.FM
//        stationData.metaData1=amfmstationinfo.frequency.toString()
//        stationData.metaData3=amfmstationinfo.stationName
//        stationData.metaData2=amfmstationinfo.rdsStationInfo
//        stationData.isFavorite= if(favoriteList.contains(stationData.metaData1))true else false

        aMFMStationInfo_t.set(AMFMStationInfo_t(amfmstationinfo.frequency,amfmstationinfo.stationName,0f,amfmstationinfo.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus(), if(favoriteList.contains(amfmstationinfo.frequency.toString()))true else false))

        DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.set(amfmstationinfo.frequency.toString())
        DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_STATIONNAME.set(amfmstationinfo.stationName)
        DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_OBJECTID.set(amfmstationinfo.objectId);
        DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY.set(0F);
        DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS.set(amfmstationinfo.tpStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP);
        DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_RDSSTATUS.set(amfmstationinfo.rdsStatus.AMFM_RDS_AVAILABLE);
        DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO.set(amfmstationinfo.rdsStationInfo)

        SystemListener.updateCarouselView(DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString(), NOWPLAYING_SOURCE_TYPE.FM)
        //changeBackground(NOWPLAYING_SOURCE_TYPE.FM.value.toInt(),DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat()) TODO

        updateRadioPresetsList(aMFMStationInfo_t.get()!!)

        //SystemListener.updateCarouselView((stationData.metaData1!!.toFloat()).toString(), NOWPLAYING_SOURCE_TYPE.FM)
        //changeBackground(stationData.sourceType.value.toInt(),(stationData.metaData1).toFloat())
        //changeBackground(1,amfmstationinfo.frequency)

    }

    //Need to be called from SDK Manager
    fun onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(amfmtuneinfo : AMFMTuneInfo_t) {
        DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set(amfmtuneinfo.freq.toString())
        DataPoolDataHandler.AMTUNER_DIRECTTUNE_AVAILABLE_KEYS.set(amfmtuneinfo.keysEnabled.toString())
    }

    fun onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(amfmtuneinfo : AMFMTuneInfo_t) {
        DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set(amfmtuneinfo.freq.toString())
        DataPoolDataHandler.FMTUNER_DIRECTTUNE_AVAILABLE_KEYS.set(amfmtuneinfo.keysEnabled.toString())
    }

    fun onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist : AMFMStationList_t) {
        DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_LISTID.set(amfmstationlist.stationlistCount);
        DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS.set(0F);
    }

    fun onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist : AMFMStationList_t) {
        DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_LISTID.set(amfmstationlist.stationlistCount);
        DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS.set(0F);
    }

    fun onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(pData : Int) {
        DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS.set(pData);
    }

    fun onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(pData : Int) {
        DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS.set(pData);
    }

    fun onAMFM_RES_FMRDSSWITCH(pData : Int) {
        DataPoolDataHandler.FMTUNER_RDSSWITCH.set(pData);
    }

    fun onAMFM_RES_REGIONSETTING(pData : Int) {
        DataPoolDataHandler.FMTUNER_REGIONSETTING.set(pData);
    }

    fun onAMFM_RES_TPSTATUS(amfmtpstatus : AMFMTPStatus_t) {
        DataPoolDataHandler.FMTUNER_TPSTATUS.set(amfmtpstatus.status);
        DataPoolDataHandler.FMTUNER_TPSTATUS_NOWPLAYING.set(0F);
        DataPoolDataHandler.FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING.set(0F);
        DataPoolDataHandler.FMTUNER_TPSTATUS_SEARCHSTATE.set(0F);
    }

    fun onAMFM_RES_TRAFFICALERT(pData : String) {
        DataPoolDataHandler.FMTUNER_TRAFFICALERT_NAME.set(pData);
    }
    fun onAMFM_RES_TRAFFICALERTACTIVECALL(amfmtrafficinfo : AMFMTrafficInfo_t) {
        DataPoolDataHandler.FMTUNER_TRAFFICALERTACTIVECALL_NAME.set(amfmtrafficinfo.name);
        DataPoolDataHandler.FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER.set(amfmtrafficinfo.programIdentifier);
    }
    fun onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(amfmprogramtypealert : AMFMProgramTypeAlert_t) {
        DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME.set(amfmprogramtypealert.name);
        DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT.set(amfmprogramtypealert.content);
    }
    fun onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED(pData : String) {
    }

    fun onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED(pData : String) {
    }

    fun onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE(pData : String) {
    }

    fun onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE(pData : String) {
    }

    fun onAMFM_RES_TRAFFICALERT_END(pData : String) {
    }

    fun onAMFM_RES_TRAFFICINFOSTART(pData : String) {
    }

    fun onAMFM_RES_ACTIONUNAVAILABLE(pData : String) {
    }
    fun onAMFM_RES_STATIONAVAILABALITYSTATUS(fmstationavailabilityinfo		 : FMStationAvailabilityInfo_t		) {
        DataPoolDataHandler.FMTUNER_STATIONAVAILABILITY_METADATA.set(fmstationavailabilityinfo.metadata);
        DataPoolDataHandler.FMTUNER_STATIONAVAILABILITY_STATUS.set(0F);
    }

    fun onFavListChange(favList:List<String>){
        favoriteList.clear()
        favoriteList.addAll(favList)
    }

    fun onDeviceChanged(iDevice: IDevice) {
        val dData = DeviceData()
        dData.type = iDevice.type.toString()
        dData.id = iDevice.objectID.toString()
        dData.isReadable = iDevice.deviceMightBePlayable.toString()
        dData.isBrowsable = iDevice.canBrowse().toString()
        if (iDevice.volumeName.isNotEmpty()) {
            dData.name = iDevice.volumeName.toString()
        }
        deviceData.set(dData)
    }

    fun changeBackground(mCurrentRadioBand: Int, mCurrentChannelNumber: Float ){

        //val hexColor = String.format("#%06X", 0xFFFFFF and RadioChannelColorMapper.getInstance(GMAudioApp.activityContext).getColorForStation(mCurrentRadioBand, mCurrentChannelNumber))
        //val color = Integer.parseInt(hexColor.replaceFirst("^#", ""), 16)
       DataPoolDataHandler.color.set(RadioChannelColorMapper.getInstance(GMAudioApp.activityContext).getColorForStation(mCurrentRadioBand, mCurrentChannelNumber))
        //print("Pavan:: mCurrentChannelNumber::"+mCurrentChannelNumber+"::Hex value::"+hexColor+"int value:"+RadioChannelColorMapper.getInstance(GMAudioApp.activityContext).getColorForStation(mCurrentRadioBand, mCurrentChannelNumber))

    }

    fun setCarouselView() {

        if(DataPoolDataHandler.aMFMStationInfo_t.get()?.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.updateCarouselView(DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString(), NOWPLAYING_SOURCE_TYPE.AM)
            else if(DataPoolDataHandler.aMFMStationInfo_t.get()?.rdsStationInfo==NOWPLAYING_SOURCE_TYPE.FM.name)
            SystemListener.updateCarouselView(DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString(), NOWPLAYING_SOURCE_TYPE.FM)

    }

    fun updateCarouselView(frequency: String, band: NOWPLAYING_SOURCE_TYPE) {

        if(!(carouselRecyclerViewAdapter==null))
        if (!carouselRecyclerViewAdapter!!.isScrolling) {
            for (position in browseList.indices) {
                if (frequency.equals(browseList[position].frequency.toString())) {
                    (carouselRecyclerViewAdapter?.recyclerView?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(position, 0)
                    carouselRecyclerViewAdapter?.updateVisibleItem(position, true)

                    break
                }
            }
        }
    }

    fun updateRadioPresetsList(stationInfo: AMFMStationInfo_t) {

        if(!(GMAudioApp.radioPresetsFragment==null)){
            for (position in browseList.indices) {
                if (stationInfo.frequency.toString().equals(browseList[position].frequency.toString())) {
                    DataPoolDataHandler.browseList[position] = stationInfo
                    break
                }
            }
        }
    }

    fun updateFavorites(stationInfo: AMFMStationInfo_t){

        for (index in browseList.indices) {
            val nowPlayingData = browseList[index]
            if (nowPlayingData.frequency.toString().equals(stationInfo.frequency.toString())) {
                if (stationInfo.isFavorite){
                    stationInfo.isFavorite = false
                    DataPoolDataHandler.browseList[index] = stationInfo
                    DataPoolDataHandler.favoriteList.remove(stationInfo!!.frequency.toString())
                }else{
                    DataPoolDataHandler.favoriteList.add(stationInfo!!.frequency.toString())
                    stationInfo.isFavorite = true
                    DataPoolDataHandler.browseList[index] = stationInfo
                }

                if (nowPlayingData.frequency.toString().equals(DataPoolDataHandler.aMFMStationInfo_t.get()!!.frequency.toString())) {
                    DataPoolDataHandler.aMFMStationInfo_t.set(AMFMStationInfo_t(stationInfo.frequency,stationInfo.stationName,0f,stationInfo.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus(), stationInfo.isFavorite))
                }

                break
            }
        }


//        DataPoolDataHandler.browseList.forEachIndexed({index, nowPlayingData->
//            kotlin.run {
//                if (nowPlayingData.frequency.toString().equals(stationInfo.frequency.toString())) {
//                    if (stationInfo.isFavorite){
//                        stationInfo.isFavorite = false
//                        DataPoolDataHandler.browseList[index] = stationInfo
//                        DataPoolDataHandler.favoriteList.remove(stationInfo!!.frequency.toString())
//                    }else{
//                        DataPoolDataHandler.favoriteList.add(stationInfo!!.frequency.toString())
//                        stationInfo.isFavorite = true
//                        DataPoolDataHandler.browseList[index] = stationInfo
//                    }
//
//                    if (nowPlayingData.frequency.toString().equals(DataPoolDataHandler.aMFMStationInfo_t.get()!!.frequency.toString())) {
////                        var stationData: AMFMStationInfo_t=AMFMStationInfo_t()
////                        stationData.sourceType=stationInfo.sourceType
////                        stationData.metaData1=stationInfo.metaData1
////                        stationData.metaData3=stationInfo.metaData3
////                        stationData.metaData2=stationInfo.metaData2
////                        stationData.isFavorite= stationInfo.isFavorite
//                        DataPoolDataHandler.aMFMStationInfo_t.set(AMFMStationInfo_t(stationInfo.frequency,stationInfo.stationName,0f,stationInfo.rdsStationInfo,0f, eRdsStatus(), eTPStationStatus(), stationInfo.isFavorite))
//                    }
//                }
//            }
//        })
    }
}