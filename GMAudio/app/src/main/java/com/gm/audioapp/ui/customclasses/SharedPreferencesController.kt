package com.gm.audioapp.ui.customclasses

import android.content.Context.MODE_PRIVATE
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.viewmodels.DataPoolDataHandler.favoriteList


class SharedPreferencesController {

    companion object {
        val SHARED_PREFS_FILE = "GMAudio_SharedPreferences"
        val FAVORITES_KEY = "Favorites_Key"

        val prefs = GMAudioApp.appContext.getSharedPreferences(SHARED_PREFS_FILE, MODE_PRIVATE)
        val editor = prefs.edit()


        fun loadPreferences() {

            val set = prefs.getStringSet(FAVORITES_KEY, null)
            if (set != null && !set.isEmpty()) {
                favoriteList.clear()
                set.forEach({
                    favoriteList.add(it)
                })
            }

        }

        fun savePreferences() {

            val set = HashSet<String>()
            set.addAll(favoriteList)
            editor.putStringSet(FAVORITES_KEY, set)
            editor.commit()
            editor.apply()
        }
    }
}