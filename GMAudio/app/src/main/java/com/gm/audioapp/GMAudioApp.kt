package com.gm.audioapp

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.support.multidex.MultiDex
import com.gm.audioapp.navigator.ActivityNavigator
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.audioapp.ui.activities.fragments.RadioPresetsFragment
import com.gm.audioapp.ui.animations.RadioAnimationManager
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.voice.VoiceLifeCycle
import java.util.*


/**
 *
 * Created by GM on 3/9/2018.
 */

class GMAudioApp : Application(), Application.ActivityLifecycleCallbacks {


    var voiceLifeCycle: VoiceLifeCycle ?= null

    override fun onCreate() {
        super.onCreate()
        appContext = this
        //activityContext = NowPlayingActivity()
        registerActivityLifecycleCallbacks(this)
        DataPoolDataHandler.themeType.set(true)
    }
    companion object {
        lateinit var appContext: GMAudioApp
        var navigator= ActivityNavigator()
        var animationManager: RadioAnimationManager? = null
        var radioPresetsFragment: RadioPresetsFragment? = null
        var useCadillacTheme: Boolean? = false
        lateinit var activityContext: Activity
        lateinit var activityContext1: NowPlayingActivity
    }

    private fun getVoiceLifeCycle(activity: Activity?): VoiceLifeCycle {
        if (voiceLifeCycle == null) {
            voiceLifeCycle = VoiceLifeCycle(activity)
        }
        return voiceLifeCycle!!
    }

    override fun onActivityPaused(activity: Activity?) {
        if (voiceLifeCycle != null) {
            voiceLifeCycle!!.stopListening()
            voiceLifeCycle = null
        }
    }

    override fun onActivityResumed(activity: Activity?) {

        getVoiceLifeCycle(activity)
        voiceLifeCycle!!.requestPermission()
        GMAudioApp.activityContext=activity!!

    }

    override fun onActivityStarted(activity: Activity?) {
    }

    override fun onActivityDestroyed(activity: Activity?) {
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityStopped(activity: Activity?) {
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        getVoiceLifeCycle(activity)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun changeLayoutDirection(localString: String){
        val locale = Locale(localString)
        val resources = getResources()
        val config = resources.getConfiguration()
        config.setLocale(locale)
    }
}