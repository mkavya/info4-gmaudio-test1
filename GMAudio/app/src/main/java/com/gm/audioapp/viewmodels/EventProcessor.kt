package com.gm.audioapp.viewmodels

import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.GMAudioApp.Companion.appContext
import com.gm.audioapp.GMAudioApp.Companion.navigator
import com.gm.audioapp.R
import com.gm.audioapp.sdkintegration.SystemController
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader


object EventProcessor {

    var eventTable = HashMap<String, Any>()
    var touchViewCommands = HashMap<String, Any>()
    var voiceCommands = HashMap<String, Any>()
    var keyboardCommands = HashMap<String, Any>()
    var remoteCommands = HashMap<String, Any>()
    private const val resFileCommands:Int=R.raw.commands
    private const val resFileEventMap:Int=R.raw.eventmap

    init {
        touchViewCommands = getCommands(resFileCommands,"touch")
        voiceCommands = getCommands(resFileCommands,"voice")
        keyboardCommands= getCommands(resFileCommands,"keyboard")
        remoteCommands= getCommands(resFileCommands,"remote")
        eventTable = getEventMapTable(resFileEventMap)
    }
    private fun getCommands(resFileRead:Int,commandType:String): HashMap<String, Any> {
        val jsonObj = JSONObject(getJsonString(appContext.resources.openRawResource(resFileRead))).getJSONObject(commandType)
        return hashMap(jsonObj)
    }
    private fun getEventMapTable(resFileRead:Int): HashMap<String, Any> {
        val jsonObj = JSONObject(getJsonString(appContext.resources.openRawResource(resFileRead)))
        return hashMap(jsonObj)
    }

    private fun hashMap(jsonObj: JSONObject): HashMap<String, Any> {
        val map = HashMap<String, Any>()
        val iterator = jsonObj.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            try {
                val command = jsonObj.get(key)
                map[key] = command
            } catch (e: JSONException) {
            }

        }
        return map
    }

    fun getJsonString(stream: InputStream): String {

        val strBuilder = StringBuilder()
        var jsonString: String? = null
        val bfReader = BufferedReader(InputStreamReader(stream, "UTF-8"))
        while ({ jsonString = bfReader.readLine(); jsonString }() != null) {
            strBuilder.append(jsonString)
        }
        return strBuilder.toString()
    }

    fun getResourceIDValue(resName: String, resIdentifier: String): Int {
        return GMAudioApp.appContext.resources.getIdentifier(resName, resIdentifier, appContext.packageName)
    }

    fun getResourceName(id: Int): String {
        return appContext.resources.getResourceEntryName(id)
    }

    fun triggerEvent(eventType: String, eventName: String, any: Any?) {
        when (eventType) {
            "navigate" -> {
                println("eventProcessor::navigate $eventName")
                navigateScreen(eventName, any)
            }
            "process" -> {
                println("eventProcessor::process $eventName")
                processEvent(eventName, any)
            }
            "finish" -> {
                println("eventProcessor::finish $eventName")
                navigator.finishActivity()
            }
            "fragment" -> {
                println("eventProcessor::fragment $eventName")
                replaceScreen(eventName, any)
            }
            "process_fragment" -> {
                println("eventProcessor::fragment $eventName")
                processEvent(eventName, any)
                replaceScreen(eventName, any)
            }
        }
    }

    fun processEvent(eventName: String, any: Any?) {
        println("processEvent::process $eventName")
        Thread().run{
            SystemController.execute(eventName, any)
        }

    }

    private fun navigateScreen(eventName: String, any: Any?) {
        println("navigateScreen::navigate $eventName")
        navigator.triggerActivity(eventName, any)
    }

    fun replaceScreen(eventName: String, any: Any?) {
        println("navigateScreen::navigate $eventName")
        if (GMAudioApp.animationManager != null)
            GMAudioApp.animationManager?.playExitAnimation(GMAudioApp.radioPresetsFragment!!)
        else
            navigator.triggerFragment(eventName, any)
    }
}