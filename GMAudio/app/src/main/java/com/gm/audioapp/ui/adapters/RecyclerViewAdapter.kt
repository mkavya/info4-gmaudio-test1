package com.gm.audioapp.ui.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gm.audioapp.BR
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler


/**
 *
 * Created by Aswin on 3/22/2018.
 */
class RecyclerViewAdapter(private var mListObjects: List<*>?, private val mChildLayoutId: Int) : RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {
    private var mInflater: LayoutInflater? = null


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    inner class RecyclerViewHolder(// each data item is just a string in this case
            private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Any, position: Int) {
            binding.setVariable(BR.obj, item)
            binding.setVariable(BR.clickHandler, EventHandler)
            binding.setVariable(BR.position, position)
            binding.setVariable(BR.dataPoolHandler, DataPoolDataHandler)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.RecyclerViewHolder {

        if (mInflater == null) {
            mInflater = parent.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        val binding = DataBindingUtil.inflate<ViewDataBinding>(mInflater!!, mChildLayoutId, parent, false)

        return RecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter.RecyclerViewHolder, position: Int) {
        val item = mListObjects!![position]
        holder.bind(item!!, position)
    }

    override fun getItemCount(): Int {
        return mListObjects!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}