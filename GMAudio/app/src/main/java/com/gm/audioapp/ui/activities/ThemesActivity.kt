package com.gm.audioapp.ui.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R

import com.gm.audioapp.databinding.ActivityThemeBinding
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler

/**
 *
 * Created by Aswin on 3/12/2018.
 */


class ThemesActivity: BaseActivity() {
    var binding: ActivityThemeBinding?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.ics_themes)
        //GMAudioApp.activityContext = this@ThemesActivity
        binding.let {

            it?.clickHandler = EventHandler
            it?.dataPoolHandler = DataPoolDataHandler
        }



    }



}