package com.gm.audioapp.ui.customclasses

import android.content.Context
import android.media.session.PlaybackState
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView

import com.android.car.apps.common.FabDrawable
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R

/**
 * An [ImageView] that renders a play/pause button like a floating action button.
 */
class PlayPauseButton(context: Context, attrs: AttributeSet) : AppCompatImageView(context, attrs) {

    private val STATE_PLAYING = intArrayOf(R.attr.state_playing)
    private val STATE_PAUSED = intArrayOf(R.attr.state_paused)

    private var mPlaybackState = -1

    init {

        val fabDrawable = FabDrawable(context)
//ManC        fabDrawable.setFabAndStrokeColor(context.getColor(R.color.car_radio_accent_color));
        //ManA Theme based changes
        val a = context.obtainStyledAttributes(intArrayOf(com.android.gm.ui.R.attr.gm_button_fab_color))
        var fabColor = context.getColor(R.color.car_radio_accent_color)
        try {
            fabColor = a.getColor(com.android.gm.ui.R.styleable.GMButtonColorTheme_gm_button_fab_color, context.getColor(R.color.car_radio_accent_color))

            if (GMAudioApp.useCadillacTheme!!) {
                fabColor = GMAudioApp.appContext.getColor(R.color.car_radio_accent_color)
            } else {
                fabColor = GMAudioApp.appContext.getColor(R.color.gm_blue_grey_700)
            }
        } finally {
            a.recycle()
        }
        fabDrawable.setFabAndStrokeColor(fabColor)

        background = fabDrawable
    }

    /**
     * Set the current play state of the button.
     *
     * @param playState One of the values from [PlaybackState]. Only
     * [PlaybackState.STATE_PAUSED] and [PlaybackState.STATE_PLAYING]
     * are valid.
     */
    fun setPlayState(playState: Int) {
        if (playState != PlaybackState.STATE_PAUSED && playState != PlaybackState.STATE_PLAYING) {
            throw IllegalArgumentException("Playback state should be either " + "PlaybackState.STATE_PAUSED or PlaybackState.STATE_PLAYING")
        }

        mPlaybackState = playState
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        // + 1 so we can potentially add our custom PlayState
        val drawableState = super.onCreateDrawableState(extraSpace + 1)

        when (mPlaybackState) {
            PlaybackState.STATE_PLAYING -> View.mergeDrawableStates(drawableState, STATE_PLAYING)
            PlaybackState.STATE_PAUSED -> View.mergeDrawableStates(drawableState, STATE_PAUSED)
        }
        if (background != null) {
            background.state = drawableState
        }
        return drawableState
    }
}
