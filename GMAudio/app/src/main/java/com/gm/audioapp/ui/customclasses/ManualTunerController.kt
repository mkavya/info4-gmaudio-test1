package com.gm.audioapp.ui.customclasses

import android.text.TextUtils
import android.view.View
import android.widget.Button
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.ManualTuneData
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE

object ManualTunerController {

    const val eventName: String = "onAMFMTuneRequest"

    private const val mNumberZero: String = "0"
    private const val mNumberOne: String = "1"
    private const val mNumberFive: String = "5"
    private const val mNumberEight: String = "8"
    private const val mNumberNine: String = "9"
    private const val mPeriod: String= "."

    private const val AM_LOWER_LIMIT = 530
    private const val AM_UPPER_LIMIT = 1700

    private const val FM_LOWER_LIMIT = 87900
    private const val FM_UPPER_LIMIT = 107900

    /**
     * The value including the decimal point of the FM upper limit.
     */
    private const val FM_UPPER_LIMIT_CHARACTERISTIC = "107."

    /**
     * The lower limit of FM channels in kilohertz before the decimal point.
     */
    private const val FM_LOWER_LIMIT_NO_DECIMAL_KHZ = 87

    private const val KILOHERTZ_CONVERSION_DIGITS = "000"
    private const val KILOHERTZ_CONVERSION_DIGITS_WITH_DECIMAL = "00"

    private var mChannelValidator: ChannelValidator? = null
    private val mAmChannelValidator = AmChannelValidator()
    private val mFmChannelValidator = FMChannelValidator()


    init {
        validateSourceType(DataPoolDataHandler.aMFMStationInfo_t.get()!!.rdsStationInfo)
    }

    fun validateSourceType(name: String) {

        mChannelValidator = if (name == NOWPLAYING_SOURCE_TYPE.AM.name)
            mAmChannelValidator
        else
            mFmChannelValidator

        updateAllButtons(StringBuilder())
    }


    interface ChannelValidator {
        fun canAppendCharacterToNumber(character: String, number: String): Boolean

        fun isValidChannel(number: String): Boolean

        fun convertToHz(number: String): Int

        fun shouldAppendPeriod(number: String): Boolean
    }

    private fun updateAllButtons(mCurrentChannel: StringBuilder){

        val buttonValues = ManualTuneData().buttonValues
        buttonValues.forEachIndexed { index, _ ->
            run {

                val enabled = mChannelValidator!!.canAppendCharacterToNumber(index.toString(), mCurrentChannel.toString())
                buttonValues[index] = enabled
            }
        }
        DataPoolDataHandler.manualTuneData.set(ManualTuneData(mCurrentChannel.toString(), isChannelValid(mCurrentChannel), buttonValues))
    }

    private fun isChannelValid(mCurrentChannel: StringBuilder): Boolean{
        if (!TextUtils.isEmpty(mCurrentChannel.toString())) {
           return mChannelValidator!!.isValidChannel(mCurrentChannel.toString())
        }

        return false
    }

    fun updateButtonState(view: View, frequency: String) {
        val mCurrentChannel = StringBuilder()
        mCurrentChannel.append(frequency)

        val button = view as Button
        mCurrentChannel.append(button.text.toString())

        if (!TextUtils.isEmpty(mCurrentChannel.toString()) && mChannelValidator!!.shouldAppendPeriod(mCurrentChannel.toString())) {
            mCurrentChannel.append(mPeriod)
        }

        updateAllButtons(mCurrentChannel)

    }

    fun deleteLastCharacterIfPeriod(view: View, frequency: String) {
        val mCurrentChannel = StringBuilder()
        mCurrentChannel.append(frequency)

        if (mCurrentChannel.isEmpty()) {
            return
        }

        val lastIndex = mCurrentChannel.length - 1
        val lastCharacter = mCurrentChannel[lastIndex].toString()

        if (lastCharacter == mPeriod) {
            mCurrentChannel.deleteCharAt(lastIndex)
        }

        mCurrentChannel.deleteCharAt(mCurrentChannel.length - 1)

        updateAllButtons(mCurrentChannel)
    }

    class AmChannelValidator : ChannelValidator {

        override fun canAppendCharacterToNumber(character: String,
                                                number: String): Boolean {
            if (character == mPeriod) {
                return false
            }

            val charValue = Integer.valueOf(character)

            when (number.length) {
                0 ->
                    return charValue >= 5 || charValue == 1
                1 ->
                    return number != mNumberFive || charValue >= 3
                2 -> {
                    return if (number[0].toString() == mNumberOne) {
                        true
                    } else character == mNumberZero

                }
                3 ->
                    return character == mNumberZero
                else ->
                    return false
            }
        }

        override fun isValidChannel(number: String): Boolean {
            if (number.isEmpty()) {
                return false
            }

            if (number.contains(mPeriod)) {
                return false
            }

            val value = Integer.valueOf(number)
            return value in AM_LOWER_LIMIT..AM_UPPER_LIMIT
        }

        override fun convertToHz(number: String): Int {
            return Integer.valueOf(number)
        }

        override fun shouldAppendPeriod(number: String): Boolean {
            return false
        }


    }

    /**
     * A [ChannelValidator] for the FM band. Note that this validator is for US regions.
     */
    class FMChannelValidator : ChannelValidator {

        override fun canAppendCharacterToNumber(character: String,
                                                number: String): Boolean {
            val indexOfPeriod = number.indexOf(mPeriod)

            if (character == mPeriod) {
                return if (indexOfPeriod != -1) {
                    false
                } else number.length >= 2

            }

            if (number.isEmpty()) {
                val charValue = Integer.valueOf(character)

                return charValue >= 8 || charValue == 1
            }

            if (indexOfPeriod == -1) {
                when (number.length) {
                    1 -> {
                        if (number == mNumberOne) {
                            return character == mNumberZero
                        }
                        if (number == mNumberEight) {
                            val numberValue = Integer.valueOf(character)
                            return numberValue >= 7
                        }
                        return true
                    }
                    2 ->
                        return (number[0].toString() == mNumberOne
                                && character != mNumberEight
                                && character != mNumberNine)
                    3 ->
                        return false
                    else -> return false
                }
            } else if (number.length - 1 > indexOfPeriod) {
                return false
            }

            if (number == FM_UPPER_LIMIT_CHARACTERISTIC) {
                return character == mNumberNine
            }

            val charValue = Integer.valueOf(character)
            return charValue % 2 == 1
        }

        override fun isValidChannel(number: String): Boolean {
            if (number.isEmpty()) {
                return false
            }

            val updatedNumber = convertNumberToKilohertz(number)
            val value = Integer.valueOf(updatedNumber)
            return value in FM_LOWER_LIMIT..FM_UPPER_LIMIT
        }

        override fun convertToHz(number: String): Int {
            return Integer.valueOf(convertNumberToKilohertz(number))
        }

        override fun shouldAppendPeriod(number: String): Boolean {
            if (number.contains(mPeriod)) {
                return false
            }

            val value = Integer.valueOf(number)
            return value >= FM_LOWER_LIMIT_NO_DECIMAL_KHZ
        }

        /**
         * Converts the given number to its kilohertz representation. For example, 87.9 will be
         * converted to 87900.
         */
        private fun convertNumberToKilohertz(number: String): String {
            return if (number.contains(mPeriod)) {
                number.replace(mPeriod, "") + KILOHERTZ_CONVERSION_DIGITS_WITH_DECIMAL
            } else number + KILOHERTZ_CONVERSION_DIGITS

        }

    }
}