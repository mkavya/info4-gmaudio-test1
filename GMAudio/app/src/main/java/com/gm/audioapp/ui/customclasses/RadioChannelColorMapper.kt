/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gm.audioapp.ui.customclasses

import android.content.Context
import android.support.annotation.ColorInt
import com.gm.audioapp.R
import com.gm.audioapp.viewmodels.AMFMStationInfo_t
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE

//import android.hardware.radio.RadioManager;

/**
 * A class that will take a [RadioStation] and return its corresponding color. The colors
 * for different channels can be found on go/aae-ncar-por in the radio section.
 */
class RadioChannelColorMapper private constructor(context: Context) {

    /**
     * Returns the default color for the radio.
     */
    @ColorInt
    @get:ColorInt
    val defaultColor: Int
    @ColorInt
    private val mAmRange1Color: Int
    @ColorInt
    private val mAmRange2Color: Int
    @ColorInt
    private val mAmRange3Color: Int
    @ColorInt
    private val mFmRange1Color: Int
    @ColorInt
    private val mFmRange2Color: Int
    @ColorInt
    private val mFmRange3Color: Int

    // AM values range from 530 - 1700. The following two values represent where this range is cut
    // into thirds.
    private var AM_LOW_THIRD_RANGE = 920
    private var AM_HIGH_THIRD_RANGE = 1310

    // FM values range from 87.9 - 108.1 kHz. The following two values represent where this range
    // is cut into thirds.
    private var FM_LOW_THIRD_RANGE = 94
    private var FM_HIGH_THIRD_RANGE = 101

    init {
        defaultColor = context.getColor(R.color.car_radio_bg_color)
        //ManC        mAmRange1Color = context.getColor(R.color.am_range_1_bg_color);
        //        mAmRange2Color = context.getColor(R.color.am_range_2_bg_color);
        //        mAmRange3Color = context.getColor(R.color.am_range_3_bg_color);
        //        mFmRange1Color = context.getColor(R.color.fm_range_1_bg_color);
        //        mFmRange2Color = context.getColor(R.color.fm_range_2_bg_color);
        //        mFmRange3Color = context.getColor(R.color.fm_range_3_bg_color);
        //ManA Theme based changes
        val a = context.obtainStyledAttributes(intArrayOf(R.attr.am_range_1_bg_color, R.attr.am_range_2_bg_color, R.attr.am_range_3_bg_color, R.attr.fm_range_1_bg_color, R.attr.fm_range_2_bg_color, R.attr.fm_range_3_bg_color))
        try {
            mAmRange1Color = a.getColor(R.styleable.RadioColorTheme_am_range_1_bg_color, context.getColor(R.color.gm_blue_grey_700))
            mAmRange2Color = a.getColor(R.styleable.RadioColorTheme_am_range_2_bg_color, context.getColor(R.color.gm_blue_grey_800))
            mAmRange3Color = a.getColor(R.styleable.RadioColorTheme_am_range_3_bg_color, context.getColor(R.color.gm_blue_grey_900))

            mFmRange1Color = a.getColor(R.styleable.RadioColorTheme_fm_range_1_bg_color, context.getColor(R.color.gm_indigo_800))
            mFmRange2Color = a.getColor(R.styleable.RadioColorTheme_fm_range_2_bg_color, context.getColor(R.color.gm_blue_800))
            mFmRange3Color = a.getColor(R.styleable.RadioColorTheme_fm_range_3_bg_color, context.getColor(R.color.gm_cyan_800))
        } finally {
            a.recycle()
        }


        if (DataPoolDataHandler.browseList.size > 0) {
            val index = DataPoolDataHandler.browseList.size / 3
            if (DataPoolDataHandler.aMFMStationInfo_t.get()!!.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.AM.name) {
                AM_LOW_THIRD_RANGE = (DataPoolDataHandler.browseList[index].frequency).toDouble().toInt()
                AM_HIGH_THIRD_RANGE = (DataPoolDataHandler.browseList[index + index].frequency).toDouble().toInt()
            } else {
                FM_LOW_THIRD_RANGE = (DataPoolDataHandler.browseList[index].frequency).toDouble().toInt()
                FM_HIGH_THIRD_RANGE = (DataPoolDataHandler.browseList[index + index].frequency).toDouble().toInt()
            }
        }

    }

    /**
     * Convenience method for returning a color based on a [RadioStation].
     *
     * @see .getColorForStation
     */
    @ColorInt
    fun getColorForStation(preset: AMFMStationInfo_t): Int {
        var x = 0
        if (preset.rdsStationInfo === NOWPLAYING_SOURCE_TYPE.AM.name)
            x = 1
        else if (preset.rdsStationInfo === NOWPLAYING_SOURCE_TYPE.FM.name)
            x = 2

        return getColorForStation(x, java.lang.Float.parseFloat(preset.frequency.toString()))
        // return getColorForStation(radioStation.getRadioBand(), radioStation.getChannelNumber());
    }

    /**
     * Returns the color that should be used for the given radio band and channel. If a match cannot
     * be made, then [.mDefaultColor] is returned.
     *
     * @param band One of [RadioManager]'s band values. (e.g. [RadioManager.BAND_AM].
     * @param channel The channel frequency in Hertz.
     */
    @ColorInt
    fun getColorForStation(band: Int, channel: Float): Int {
        when (band) {
        //            case RadioManager.BAND_AM:
            0 -> {
                if (channel < AM_LOW_THIRD_RANGE) {
                    return mAmRange1Color
                } else if (channel > AM_HIGH_THIRD_RANGE) {
                    return mAmRange3Color
                }

                return mAmRange2Color
            }

        //            case RadioManager.BAND_FM:
            1 -> {
                if (channel < FM_LOW_THIRD_RANGE) {
                    return mFmRange1Color
                } else if (channel > FM_HIGH_THIRD_RANGE) {
                    return mFmRange3Color
                }

                return mFmRange2Color
            }

            else -> return defaultColor
        }
    }

    @ColorInt
    fun getColorForStation(band: Int, channel: Int): Int {
        val fltChannel = channel.toFloat() / 1000

        return getColorForStation(band, fltChannel)

    }

    companion object {

        fun getInstance(context: Context): RadioChannelColorMapper {
            return RadioChannelColorMapper(context)
        }
    }
}
