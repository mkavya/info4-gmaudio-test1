package com.gm.audioapp.ui.activities

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.android.car.app.CarDrawerAdapter
import com.android.car.app.DrawerItemViewHolder
import com.android.gm.ui.GMCarDrawerAdapter
import com.android.gm.ui.GMDrawerActivity
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.GMAudioApp.Companion.activityContext1
import com.gm.audioapp.R
import com.gm.audioapp.navigator.ActivityNavigator
import com.gm.audioapp.ui.activities.fragments.FragmentWithFade
import com.gm.audioapp.ui.activities.fragments.NowPlayingFragment
import com.gm.audioapp.viewmodels.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.NOWPLAYING_SOURCE_TYPE


/**
 *
 * Created by YZPNMQ on 3/7/2018.
 */
class NowPlayingActivity : GMDrawerActivity() {

    private val SUPPORTED_RADIO_BANDS = intArrayOf(
            BAND_AM, BAND_FM)
    private val evG_Radio_Init = "initAudio"
    /**
     * Whether or not it is safe to make transactions on the
     * [FragmentManager]. This variable prevents a possible exception
     * when calling commit() on the FragmentManager.
     *
     *
     * The default value is `true` because it is only after
     * [.onSaveInstanceState] has been called that fragment commits are not allowed.
     */
    private var mAllowFragmentCommits = true
    private var navItemSelected = 0

   companion object {
       val CONTENT_FRAGMENT_TAG = "CONTENT_FRAGMENT_TAG"
       val MANUAL_TUNER_BACKSTACK = "MANUAL_TUNER_BACKSTACK"
       val BAND_AM = 1
       val BAND_FM = 2

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentFragment(NowPlayingFragment.getInstance())

    }

    private fun setContentFragment(fragment: Fragment) {
        if (!mAllowFragmentCommits) {
            return
        }
        supportFragmentManager.beginTransaction()
                .replace(contentContainerId, fragment, CONTENT_FRAGMENT_TAG)
                .commit()
    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount > 0) {
            // A station can only be selected if the manual tuner fragment has been shown; so, remove
            // that here.
            supportFragmentManager.popBackStack()

            val currentFragment = getCurrentFragment()
            if (currentFragment is FragmentWithFade) {
                (currentFragment as FragmentWithFade).fadeInContent()
            }
        }else {
            super.onBackPressed()
        }
    }

    fun getContainerId(): Int {
        return super.getContentContainerId()
    }

    override fun getRootAdapter(): CarDrawerAdapter {
        return RadioDrawerAdapter()
    }

    /**
     * An adapter that is responsible for populating the Radio drawer with the available bands to
     * select, as well as the option for opening the manual tuner.
     */
    private inner class RadioDrawerAdapter internal constructor() : GMCarDrawerAdapter(this@NowPlayingActivity, false) {
        private val mDrawerOptions = ArrayList<String>(SUPPORTED_RADIO_BANDS.size + 1)

        override fun getActualItemCount(): Int {
            return mDrawerOptions.size
        }

        init {
            setTitle(getString(R.string.ui_audio_small))
            mDrawerOptions.add(NOWPLAYING_SOURCE_TYPE.AM.name)
            mDrawerOptions.add(NOWPLAYING_SOURCE_TYPE.FM.name)
            mDrawerOptions.add(getString(R.string.themes))
        }

        override fun populateViewHolder(holder: DrawerItemViewHolder, position: Int) {
            holder.title.text = mDrawerOptions[position]
        }

        override fun onItemClick(position: Int) {
            navItemSelected = position
            closeDrawer()
            EventHandler.onDrawerChange(navItemSelected,this@NowPlayingActivity)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mAllowFragmentCommits = false
    }


    /**
     * Returns the fragment that is currently being displayed as the content view. Note that this
     * is not necessarily the fragment that is visible. The manual tuner fragment can be displayed
     * on top of this content fragment.
     */
    fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentByTag(CONTENT_FRAGMENT_TAG)
    }

    override fun getTheme(): Resources.Theme {
        val theme = super.getTheme()
        GMAudioApp.useCadillacTheme =DataPoolDataHandler.themeType.get()
        if (GMAudioApp.useCadillacTheme!!) {
            theme.applyStyle(R.style.CadillacTheme, true)
        }else{
            theme.applyStyle(R.style.NonCadillacTheme, true)
        }
        return theme
    }

    override fun onResume() {
        super.onResume()
        activityContext1 = this

    }

    override fun onStart() {
        super.onStart()
        // Fragment commits are not allowed once the Activity's state has been saved. Once
        // onStart() has been called, the FragmentManager should now allow commits.
        mAllowFragmentCommits = true
        EventHandler.initEvent(evG_Radio_Init)

    }

    override fun onNavDrawerClosed() {
        //EventHandler.onDrawerChange(navItemSelected,this@NowPlayingActivity)

    }
}